package com.innasoft.kilomart.Response;

import java.util.List;

public class HomeResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : [{"id":"1","name":"Staples","image":"images/category/main/d82ee-staples1.png"},{"id":"2","name":"Beverages","image":"images/category/main/a8bb1-beverages1.png"},{"id":"3","name":"Household Care","image":"images/category/main/2704f-householdcare1.png"},{"id":"4","name":"Personal Needs","image":"images/category/main/4b222-personlaneed1.png"},{"id":"5","name":"Baby care","image":"images/category/main/caa83-babycare1.png"},{"id":"6","name":"Packed Food","image":"images/category/main/2cfd7-packedfood1.png"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * name : Staples
         * image : images/category/main/d82ee-staples1.png
         */

        private String id;
        private String name;
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
