package com.innasoft.kilomart.Services;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.innasoft.kilomart.NotificationActivity;
import com.innasoft.kilomart.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    //there can be multiple notifications so it can be used as notification identity
    private static final String CHANNEL_ID = "KILOMART_01";
    public static final int NOTIFICATION_ID = 1;

    Bitmap bitmap;
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e("remoteMessage", "" + remoteMessage.getData());


      /*  Map<String, String> params = remoteMessage.getData();
        JSONObject object = new JSONObject(params);
        String taskId= "";
        try {
            Log.d("JSON_OBJECT", object.getString("task_id"));
            taskId = object.getString("task_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

       if (remoteMessage.getData().size() > 0){
           Log.d(TAG, "Message data payload: " + remoteMessage.getData());
           String title,message,imageUrl;

           title = remoteMessage.getData().get("title");
           message = remoteMessage.getData().get("message");
           imageUrl = remoteMessage.getData().get("imageUrl");

           bitmap = getBitmapfromUrl(imageUrl);

           Intent intent = new Intent(this,NotificationActivity.class);
           intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
           PendingIntent pendingIntent =  PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_ONE_SHOT);
           Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

           NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
                builder.setContentTitle(title)
                .setContentText(message)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                        .setStyle(new NotificationCompat.BigPictureStyle()
                                .bigPicture(bitmap))/*Notification with Image*/
                .setSound(uri)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

           NotificationManager notificationManager =
                   (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

           notificationManager.notify(0 /* ID of notification */, builder.build());

       }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }


    }

    /*
     *To get a Bitmap image from the URL received
     * */
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }

}
