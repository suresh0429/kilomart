package com.innasoft.kilomart;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.demono.AutoScrollViewPager;
import com.google.android.gms.location.LocationRequest;
import com.innasoft.kilomart.Adapters.AutoViewPager;
import com.innasoft.kilomart.Adapters.HomeAdapter;
import com.innasoft.kilomart.Adapters.LocationAdapter;
import com.innasoft.kilomart.Adapters.PopularProductAdaptre;
import com.innasoft.kilomart.Adapters.SearchAdapter;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Helper.Converter;
import com.innasoft.kilomart.Model.LocationItem;
import com.innasoft.kilomart.Reciever.ConnectivityReceiver;
import com.innasoft.kilomart.Response.AppVersionResponse;
import com.innasoft.kilomart.Response.BannersResponse;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Response.HomeResponse;
import com.innasoft.kilomart.Response.LocationsResponse;
import com.innasoft.kilomart.Response.ProductResponse;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;
import com.innasoft.kilomart.Storage.Utilities;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.kilomart.Apis.RetrofitClient.ABOUT_US_URL;
import static com.innasoft.kilomart.Apis.RetrofitClient.PRIVACY_POLICY_URL;
import static com.innasoft.kilomart.Apis.RetrofitClient.TERMS_CONDITIONS_URL;
import static com.karumi.dexter.Dexter.withActivity;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, ConnectivityReceiver.ConnectivityReceiverListener {
    boolean doubleBackToExitPressedOnce = false;
    ConnectivityReceiver connectivityReceiver;
    int color = Color.RED;
    AppController appController;
    @BindView(R.id.simpleSwipeRefreshLayout)
    SwipeRefreshLayout simpleSwipeRefreshLayout;
    @BindView(R.id.homeRecycler)
    RecyclerView homeRecycler;
    @BindView(R.id.popularRecycler)
    RecyclerView popularRecycler;
    @BindView(R.id.viewPager)
    AutoScrollViewPager viewPager;


    private PrefManager pref;
    Snackbar snackbar;
    int cartindex;
    String userId, imagePic, deviceId, tokenValue, loc_area, checnkVeriosn, android_version;
    ImageView nav_Image;
    String title = "";
    TextView nav_username, nav_useremail;

    //    private FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String, Object> firebaseDefaultMap;
    private static final String TAG = "HomeActivity";
    private static final String VERSION_CODE_KEY = "app_latest_version";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setLogo(R.drawable.kilomartlogo);
        toolbar.setContentInsetStartWithNavigation(0);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        //LandScape mode
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        appController = (AppController) getApplication();
        pref = new PrefManager(getApplicationContext());

        // Method to manually check connection status
        boolean isConnected = appController.isConnection();
        //showSnack(isConnected);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);
        nav_username = (TextView) hView.findViewById(R.id.userName);
        nav_useremail = (TextView) hView.findViewById(R.id.userEmail);
        nav_Image = (ImageView) hView.findViewById(R.id.imageView);


        // search Layout
        TextView searchLayout = (TextView) findViewById(R.id.searchLayout);
        searchLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        homeRecycler.setLayoutManager(mLayoutManager);
        homeRecycler.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        homeRecycler.setItemAnimator(new DefaultItemAnimator());

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        popularRecycler.setLayoutManager(mLayoutManager1);
        popularRecycler.setItemAnimator(new DefaultItemAnimator());


        if (isConnected) {

            simpleSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);
            simpleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    userData();
                    cartCount();
                    prepareHomeBannersData();
                    prepareHomeData();
                    popularData();
                    locationShowFirsttime();

                    simpleSwipeRefreshLayout.setRefreshing(false);


                }
            });

            userData();
            cartCount();
            prepareHomeBannersData();
            prepareHomeData();
            popularData();
            locationShowFirsttime();

            CheckappUpdate();



        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }


    }





    // user information
    private void userData() {
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        String username = profile.get("name");
        String email = profile.get("email");
        imagePic = profile.get("profilepic");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        nav_useremail.setText(email);
        nav_username.setText(username);

        if (imagePic != null && !imagePic.isEmpty() && !imagePic.equals("null")) {
            // loading album cover using Glide library
            Glide.with(getApplicationContext()).load(imagePic).into(nav_Image);
        } else {

            nav_Image.setImageResource(R.drawable.ic_user);
        }

    }

    // cart count
    private void cartCount() {


        appController.cartCount(userId, deviceId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindex", "" + cartindex);
        invalidateOptionsMenu();


    }

    // location Prefences
    private void locationShowFirsttime() {
        //location prefences
        SharedPreferences locationPref = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        boolean locationPrefBoolean = locationPref.getBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
        loc_area = locationPref.getString(Utilities.KEY_AREA, "");


        if (locationPrefBoolean) {
            showLocationsDialog();
        }
    }

    // home banners
    private void prepareHomeBannersData() {

        simpleSwipeRefreshLayout.setRefreshing(true);

        Call<BannersResponse> call = RetrofitClient.getInstance().getApi().getHomeBanners();
        call.enqueue(new Callback<BannersResponse>() {
            @Override
            public void onResponse(Call<BannersResponse> call, Response<BannersResponse> response) {
                BannersResponse bannersResponse = response.body();

                if (response.isSuccessful()) {
                    simpleSwipeRefreshLayout.setRefreshing(false);

                    if (bannersResponse.getStatus().equalsIgnoreCase("10100")) {


                        final List<BannersResponse.DataBean> homeBannersBeanList = response.body().getData();

                        AutoViewPager mAdapter = new AutoViewPager(homeBannersBeanList, HomeActivity.this);
                        viewPager.setAdapter(mAdapter);
                        // optional start auto scroll
                        viewPager.startAutoScroll();


                    } else if (bannersResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), bannersResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (bannersResponse.getStatus().equals("10300")) {

                        Toast.makeText(getApplicationContext(), bannersResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (bannersResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), bannersResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }


            }

            @Override
            public void onFailure(Call<BannersResponse> call, Throwable t) {
                simpleSwipeRefreshLayout.setRefreshing(false);
                Snackbar.make(homeRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();


            }
        });

    }

    // home modules
    private void prepareHomeData() {

        simpleSwipeRefreshLayout.setRefreshing(true);

        Call<HomeResponse> call = RetrofitClient.getInstance().getApi().getHomePageRequest("main", "", "");
        call.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                HomeResponse addressResponse = response.body();

                if (response.isSuccessful()) {
                    simpleSwipeRefreshLayout.setRefreshing(false);

                    if (addressResponse.getStatus().equalsIgnoreCase("10100")) {

                        // modules
                        final List<HomeResponse.DataBean> modulesBeanList = response.body().getData();


                        final HomeAdapter adapter = new HomeAdapter(getApplicationContext(), modulesBeanList);
                        homeRecycler.setAdapter(adapter);


                    } else if (addressResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), addressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (addressResponse.getStatus().equals("10300")) {

                        Toast.makeText(getApplicationContext(), addressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (addressResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), addressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }


            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                simpleSwipeRefreshLayout.setRefreshing(false);
                Snackbar.make(homeRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();


            }
        });

    }


    // popular products
    private void popularData() {

        simpleSwipeRefreshLayout.setRefreshing(true);

        Call<ProductResponse> call = RetrofitClient.getInstance().getApi().getPopularProduct("SINGLE", deviceId, "POPULARITY");
        //Call<ProductResponse> call = RetrofitClient.getInstance().getApi().getPopularProduct("SINGLE", deviceId, "dal");
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                ProductResponse addressResponse = response.body();

                if (response.isSuccessful()) {
                    simpleSwipeRefreshLayout.setRefreshing(false);

                    if (addressResponse.getStatus().equalsIgnoreCase("10100")) {

                        // modules
                        final List<ProductResponse.DataBean> modulesBeanList = response.body().getData();


                        final PopularProductAdaptre adapter = new PopularProductAdaptre(getApplicationContext(), modulesBeanList);
                        popularRecycler.setAdapter(adapter);


                    } else if (addressResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), addressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (addressResponse.getStatus().equals("10300")) {

                        Toast.makeText(getApplicationContext(), addressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (addressResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), addressResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                simpleSwipeRefreshLayout.setRefreshing(false);
                Snackbar.make(popularRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();


            }
        });

    }

    private void Logout() {
        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userLogout(tokenValue, userId, deviceId);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (response.isSuccessful()) ;
                BaseResponse baseResponse = response.body();

                if (baseResponse.getStatus().equals("10100")) {

                    SharedPreferences preferences = getApplicationContext().getSharedPreferences("LOCATION_PREF", 0);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.apply();
                    editor.clear();

                    pref.clearSession();
                    //Logout
                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();


                } else if (baseResponse.getStatus().equals("10200")) {

                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (baseResponse.getStatus().equals("10300")) {

                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (baseResponse.getStatus().equals("10400")) {

                    Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void CheckappUpdate() {


        PackageInfo eInfo = null;
        try {
            eInfo = getPackageManager().getPackageInfo("com.innasoft.kilomart", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        checnkVeriosn = eInfo.versionName;

        pref = new PrefManager(getApplicationContext());

        HashMap<String, String> profile = pref.getUserDetails();
        tokenValue = profile.get("AccessToken");


        Call<AppVersionResponse> call = RetrofitClient.getInstance().getApi().CheckAppUpdate(tokenValue);
        call.enqueue(new Callback<AppVersionResponse>() {
            @Override
            public void onResponse(Call<AppVersionResponse> call, Response<AppVersionResponse> response) {
                if (response.isSuccessful()) ;
                AppVersionResponse appVersionResponse = response.body();
                if (appVersionResponse.getStatus().equals("10100")) {

                    android_version = appVersionResponse.getData().getAndroidVersion();

                    Log.d(TAG, "onResponse: "+android_version+"---"+checnkVeriosn);

                    if (!android_version.equalsIgnoreCase(checnkVeriosn)) {

                        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(HomeActivity.this);
                        builder.setTitle("Update the App")
                                .setMessage("A new version of this app is available on play store. if you update click ok").setPositiveButton(
                                "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        final Uri marketUri = Uri.parse("https://play.google.com/store/apps/details?id=com.innasoft.kilomart&hl=en");
                                        startActivity(new Intent(Intent.ACTION_VIEW, marketUri));
                                    }
                                }).setNegativeButton("Later", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                builder.setCancelable(true);
                            }
                        }).setCancelable(true).show();
                    }

                } else if (appVersionResponse.getStatus().equals("10200")) {
                    Toast.makeText(HomeActivity.this, appVersionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (appVersionResponse.getStatus().equals("10300")) {
                    Toast.makeText(HomeActivity.this, appVersionResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<AppVersionResponse> call, Throwable t) {
                Toast.makeText(HomeActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

        userData();
        cartCount();
        locationShowFirsttime();

        // receiver function
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");

        connectivityReceiver = new ConnectivityReceiver();
        registerReceiver(connectivityReceiver, intentFilter);

        AppController.getInstance().setConnectivityListener(this);

    }

    @Override
    protected void onRestart() {
        super.onRestart();

        userData();
        cartCount();
        locationShowFirsttime();

    }

    @Override
    protected void onStart() {
        super.onStart();

        userData();
        cartCount();
        locationShowFirsttime();

    }

    @Override
    protected void onStop()
    {
        unregisterReceiver(connectivityReceiver);
        super.onStop();
    }


    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {

        if (!isConnected) {
            Snackbar.make(homeRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.Sorry_Notconnected_to_internet) + "</font>"), Snackbar.LENGTH_SHORT).show();


        } /*else {

            Snackbar.make(homeRecycler, Html.fromHtml("<font color=\""+Color.WHITE+"\">"+ getResources().getString(R.string.goodinternet)+"</font>"),Snackbar.LENGTH_SHORT).show();

        }*/

    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds countries to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);

        final MenuItem alertMenuItem = menu.findItem(R.id.action_location);
        RelativeLayout rootView = (RelativeLayout) alertMenuItem.getActionView();
        TextView txtArea = (TextView) rootView.findViewById(R.id.txt_area);
        txtArea.setText(loc_area);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(alertMenuItem);
            }
        });


        final MenuItem menuItem = menu.findItem(R.id.action_cart);

        menuItem.setIcon(Converter.convertLayoutToImage(HomeActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {

            Intent intent = new Intent(getApplicationContext(), CartActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            return true;
        } else if (id == R.id.action_location) {
            showLocationsDialog();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_account) {
            //My Account
            Intent intent = new Intent(HomeActivity.this, MyAccountActivity.class);
            intent.putExtra("Checkout", false);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.My_orders) {
            //My orders
            Intent intent = new Intent(HomeActivity.this, OrderIdActvity.class);
            intent.putExtra("Checkout", false);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.action_notification) {

            Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.nav_logout) {

            Logout();

        } else if (id == R.id.nav_wishlist) {

//            Intent intent = new Intent(getApplicationContext(), WishListActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(intent);
//            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

        } else if (id == R.id.nav_share) {

            String sAux ="Install Kilomart application click below link \n" +"https://play.google.com/store/apps/details?id=com.innasoft.kilomart";
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, sAux);
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);

        }   /* else if (id == R.id.nav_support) {


            Intent intent = new Intent(getApplicationContext(), SupportUsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        }*/ else if (id == R.id.nav_privacy) {

            title = "Privacy & Policy";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", PRIVACY_POLICY_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_terms) {

            title = "Terms & Conditions";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", TERMS_CONDITIONS_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_return_policy) {

            title = "Return Policy";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", "");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_faq) {

            title = "FAQ's";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", "");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (id == R.id.nav_aboutus) {

            title = "About Us";
            Intent intent = new Intent(getApplicationContext(), PdfActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", ABOUT_US_URL);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer != null) {

            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    moveTaskToBack(true);
                    Process.killProcess(Process.myPid());
                    System.exit(1);
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce=false;
                    }
                }, 2000);


            }
        }
    }


    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    private void showLocationsDialog() {

        Call<LocationsResponse> call = RetrofitClient.getInstance().getApi().getLocations();
        call.enqueue(new Callback<LocationsResponse>() {
            @Override
            public void onResponse(Call<LocationsResponse> call, retrofit2.Response<LocationsResponse> response) {

                LocationsResponse addressResponse = response.body();

                if (response.isSuccessful()) {


                    if (addressResponse.getStatus().equalsIgnoreCase("10100")) {


                        List<LocationsResponse.DataBean> locationItemsArray = response.body() != null ? response.body().getData() : null;

                        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
                        ViewGroup viewGroup = findViewById(android.R.id.content);

                        //then we will inflate the custom alert dialog xml that we created
                        View dialogView = LayoutInflater.from(HomeActivity.this).inflate(R.layout.my_location_lis_dialog, viewGroup, false);

                        //Now we need an AlertDialog.Builder object
                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);

                        //setting the view of the builder to our custom view that we already inflated
                        builder.setView(dialogView);

                        //finally creating the alert dialog and displaying it
                        AlertDialog alertDialog = builder.create();
                        alertDialog.setCancelable(true);
                        alertDialog.show();


                        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.locationRecycler);
                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(mLayoutManager1);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());

                        LocationAdapter adapter = new LocationAdapter(getApplicationContext(), locationItemsArray,alertDialog);
                        recyclerView.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<LocationsResponse> call, Throwable t) {

                Snackbar.make(popularRecycler, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

            }
        });

    }





    }
