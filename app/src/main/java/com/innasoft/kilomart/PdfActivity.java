package com.innasoft.kilomart;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PdfActivity extends AppCompatActivity {


    @BindView(R.id.webView)
    WebView webView;

    WebSettings webSettings;
    String title, url;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.txtAlert)
    TextView txtAlert;


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            title = getIntent().getStringExtra("title");
            url = getIntent().getStringExtra("url");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);

        if (title.equalsIgnoreCase("Return Policy") ||
                title.equalsIgnoreCase("FAQ's")
                || url.equalsIgnoreCase("")) {
            webView.setVisibility(View.GONE);
            txtAlert.setVisibility(View.VISIBLE);
            progress.setVisibility(View.GONE);
        }else {

            // Load data into a WebView
            webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient());
            webView.loadUrl(url);
        }




    }

    public class WebViewClient extends android.webkit.WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            /*** Hide ProgressBar while page completely load ***/
            progress.setVisibility(View.GONE);

        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}

