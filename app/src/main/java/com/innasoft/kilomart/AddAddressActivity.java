package com.innasoft.kilomart;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Reciever.ConnectivityReceiver;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;
import com.innasoft.kilomart.Storage.Utilities;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.kilomart.AddressListActivity.MY_PERMISSIONS_REQUEST_LOCATION;


public class AddAddressActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener,
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    Geocoder geocoder;
    String latitude1, longitude1;
    List<Address> addresses;
    double latitute_tt, longitiu_II;
    SupportMapFragment mapFragment;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    private GoogleMap mMap;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;

    @BindView(R.id.textSearch)
    TextView textSearch;
    @BindView(R.id.rl_map)
    RelativeLayout rlMap;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.name_til)
    TextInputLayout nameTil;
    @BindView(R.id.etAddressline1)
    EditText etAddressline1;
    @BindView(R.id.adreess1_til)
    TextInputLayout adreess1Til;
    @BindView(R.id.etAddressline2)
    EditText etAddressline2;
    @BindView(R.id.adreess2_til)
    TextInputLayout adreess2Til;
    @BindView(R.id.etArea)
    EditText etArea;
    @BindView(R.id.area_til)
    TextInputLayout areaTil;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.city_till)
    TextInputLayout cityTill;
    @BindView(R.id.etState)
    EditText etState;
    @BindView(R.id.state_til)
    TextInputLayout stateTil;
    @BindView(R.id.etCountry)
    EditText etCountry;
    @BindView(R.id.country_til)
    TextInputLayout countryTil;
    @BindView(R.id.etPincode)
    EditText etPincode;
    @BindView(R.id.zip_til)
    TextInputLayout zipTil;
    @BindView(R.id.etPhoneNo)
    EditText etPhoneNo;
    @BindView(R.id.mobile_til)
    TextInputLayout mobileTil;
    @BindView(R.id.etAlternateno)
    EditText etAlternateno;
    @BindView(R.id.ti_etAlternateno)
    TextInputLayout tiEtAlternateno;
    @BindView(R.id.checkboxDefault)
    CheckBox checkboxDefault;
    @BindView(R.id.btnApply)
    Button btnApply;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;

    private static final String TAG = "AddAddressActivity";
    AppController appController;


    private PrefManager pref;
    String userId, checkId, loc_area, loc_pincode,shippingcaharge, tokenValue, deviceId, username, mobile;
    boolean checkoutStatus,locationPrefBoolean;

    private static final LatLngBounds BOUNDS_INDIA = new LatLngBounds(new LatLng(23.63936, 68.14712), new LatLng(28.20453, 97.34466));


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Address");


        geocoder = new Geocoder(this, Locale.getDefault());

        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        username = profile.get("name");
        mobile = profile.get("mobile");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        // location Prefences
        SharedPreferences locationPref = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
         locationPrefBoolean = locationPref.getBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
         loc_area = locationPref.getString(Utilities.KEY_AREA, "");
         loc_pincode = locationPref.getString(Utilities.KEY_PINCODE, "");
         shippingcaharge = locationPref.getString(Utilities.KEY_SHIPPINGCHARGES, "");


        if (getIntent().getExtras() != null) {
            checkoutStatus = getIntent().getBooleanExtra("Checkout", false);

        }

        appController = (AppController) getApplication();


        etName.setText(username);
        etPhoneNo.setText(mobile);
        etArea.setText(loc_area);
        etPincode.setText(loc_pincode);
        etCity.setText("Hyderabad");
        etState.setText("Telangana");
        etCountry.setText("India");

        etName.addTextChangedListener(new MyTextWatcher(etName));
        etAddressline1.addTextChangedListener(new MyTextWatcher(etAddressline1));
        etAddressline2.addTextChangedListener(new MyTextWatcher(etAddressline2));
        etArea.addTextChangedListener(new MyTextWatcher(etArea));
        etCity.addTextChangedListener(new MyTextWatcher(etCity));
        etState.addTextChangedListener(new MyTextWatcher(etState));
        etCountry.addTextChangedListener(new MyTextWatcher(etCountry));
        etPincode.addTextChangedListener(new MyTextWatcher(etPincode));
        etPhoneNo.addTextChangedListener(new MyTextWatcher(etPhoneNo));
        etAlternateno.addTextChangedListener(new MyTextWatcher(etAlternateno));


        checkId = "Yes";
        checkboxDefault.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    checkId = "Yes";
                } else {

                    checkId = "No";
                }
            }
        });

        textSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new PlaceAutocomplete.IntentBuilder
                            (PlaceAutocomplete.MODE_FULLSCREEN)
                            .setBoundsBias(BOUNDS_INDIA)
                            .build(AddAddressActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException |
                        GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        configureCameraIdle();


    }


    @OnClick(R.id.btnApply)
    public void onViewClicked(View view) {

        switch (view.getId()) {
            case R.id.btnApply:

                boolean isConnected = appController.isConnection();
                if (isConnected) {
                    validateForm();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;
                    snackBar(message, color);
                }


                break;
        }


    }


    //validate inputs...
    private void validateForm() {

        String name = etName.getText().toString();
        String addressline1 = etAddressline1.getText().toString();
        String addressline2 = etAddressline2.getText().toString();
        final String area = etArea.getText().toString();
        String city = etCity.getText().toString();
        String state = etState.getText().toString();
        String country = etCountry.getText().toString();
        final String pincode = etPincode.getText().toString();
        String phone = etPhoneNo.getText().toString();
        String alternateno = etAlternateno.getText().toString();


        if ((!isValidName(name))) {
            return;
        }
        if (!isValidPinCode(pincode)) {
            return;
        }
        if ((!isValidPhoneNumber(phone))) {
            return;
        }
        /*if ((!isValidPhoneNumber1(alternateno))) {
            return;
        }
*/


        progressBar.setVisibility(View.VISIBLE);
        btnApply.setEnabled(false);
        btnApply.setVisibility(View.GONE);

      //  Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userAddress(tokenValue, userId, name, addressline1, addressline2, area, city, state, pincode, phone, alternateno, String.valueOf(latitute_tt), String.valueOf(longitiu_II), checkId);
        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userAddress(tokenValue, userId, name, addressline1, addressline2, area, city, state, pincode, phone, alternateno, "", "", checkId);

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                progressBar.setVisibility(View.GONE);
                btnApply.setEnabled(true);
                btnApply.setVisibility(View.VISIBLE);

                final BaseResponse registerResponse = response.body();

                if (response.isSuccessful()) {

                    if (registerResponse.getStatus().equalsIgnoreCase("10100")) {




                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    Intent intent = new Intent(AddAddressActivity.this, AddressListActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.putExtra("Checkout", checkoutStatus);
                                    startActivity(intent);


                                }
                            }, 1000);


                    }

                } else {
                    int color = Color.RED;
                    snackBar(registerResponse.getMessage(), color);
                }


            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btnApply.setEnabled(true);
                btnApply.setVisibility(View.VISIBLE);

            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }


    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }


    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    // validate name
    private boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(name);

        if (name.isEmpty()) {
            nameTil.setError("name is required");
            requestFocus(etName);
            return false;
        } else if (!matcher.matches()) {
            nameTil.setError("Enter Alphabets Only");
            requestFocus(etName);
            return false;
        } else if (name.length() < 5 || name.length() > 20) {
            nameTil.setError("Name Should be 5 to 20 characters");
            requestFocus(etName);
            return false;
        } else {
            nameTil.setErrorEnabled(false);
        }
        return matcher.matches();
    }


    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            mobileTil.setError("Phone no is required");
            requestFocus(etPhoneNo);
            return false;
        } else if (!matcher.matches()) {
            mobileTil.setError("Enter a valid mobile");
            requestFocus(etPhoneNo);
            return false;
        } else {
            mobileTil.setErrorEnabled(false);
        }

        return matcher.matches();
    }

    private boolean isValidPhoneNumber1(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            tiEtAlternateno.setError("Phone no is required");
            requestFocus(etAlternateno);
            return false;
        } else if (!matcher.matches()) {
            tiEtAlternateno.setError("Enter a valid mobile");
            requestFocus(etAlternateno);
            return false;
        } else {
            tiEtAlternateno.setErrorEnabled(false);
        }

        return matcher.matches();
    }

    // valid OTP
    private boolean isValidPinCode(String pincode) {


        if (pincode.isEmpty()) {
            zipTil.setError("Pincode is required");
            requestFocus(etPincode);
            etCity.setText("");
            etState.setText("");
            return false;
        } else if (pincode.length() < 6) {
            zipTil.setError("Enter a valid Pincode");

            return false;
        } else {
            zipTil.setErrorEnabled(false);
        }

        return true;
    }


    // request focus
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etName:
                    isValidName(etName.getText().toString().trim());
                    break;
                case R.id.etPhoneNo:
                    isValidPhoneNumber(etPhoneNo.getText().toString().trim());
                    break;
                case R.id.etPincode:
                    isValidPinCode(etPincode.getText().toString().trim());
                    break;
                case R.id.etAlternateno:
                    isValidPhoneNumber1(etAlternateno.getText().toString().trim());
                    break;

            }
        }
    }


    private void configureCameraIdle() {
        onCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {

                LatLng latLng = mMap.getCameraPosition().target;
                Geocoder geocoder = new Geocoder(AddAddressActivity.this);

                try {
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        String locality = addressList.get(0).getAddressLine(0);  //whole address coming in lovality
                        String addr[] = locality.split(",", 2);
                        Log.d("LOClity", locality);
                        Log.d("ARYA", addr[0] + "////" + addr[1]);

                        String country = addressList.get(0).getCountryName();
                        String state = addressList.get(0).getAdminArea();
                        String city = addressList.get(0).getLocality();
                        String area = addressList.get(0).getSubLocality();
                        String pincode = addressList.get(0).getPostalCode();
                        latitute_tt = addressList.get(0).getLatitude();
                        longitiu_II = addressList.get(0).getLongitude();
                        if (!locality.isEmpty() && !country.isEmpty())
                            etName.setText(username);
                        etAddressline1.setText(addr[0]);
                        etAddressline2.setText(addr[1]);
                        etArea.setText(area);
                        etCity.setText(city);
                        etState.setText(state);
                        etCountry.setText(country);
                        etPincode.setText(pincode);
                        etPhoneNo.setText(mobile);

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        };
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
      /*  markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);
*/
        //  CircleOptions addCircle = new CircleOptions().center(latLng).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        //   mCircle = mMap.addCircle(addCircle);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(19));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnCameraIdleListener(onCameraIdleListener);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(AddAddressActivity.this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode ==
                PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.d("EDDIPPPP", "" + place.getAddress() + "---" + place.getLatLng());
                String stringlat = place.getLatLng().toString();
                CharSequence name = place.getName();
                final CharSequence address = place.getAddress();
                String attributions = (String) place.getAttributions();
                if (attributions == null) {
                    attributions = "";
                }
                if (name.toString().contains("°")) {
                    name = "";
                }
                stringlat = stringlat.substring(stringlat.indexOf("(") + 1);
                stringlat = stringlat.substring(0, stringlat.indexOf(")"));
                String latValue = stringlat.split(",")[0];
                latitude1 = latValue;
                String lngValue = stringlat.split(",")[1];
                longitude1 = lngValue;

                Double lat = Double.valueOf(latitude1);
                Double lng = Double.valueOf(longitude1);
                String fulladd = address.toString();
                String addr[] = fulladd.split(",", 2);
                if (name != null && name.length() > 0)
                    etAddressline1.setText(name);
                else
                    etAddressline2.setText(addr[0]);


                //area_edt.setText(addr[5]+","+addr[6]+","+addr[7]);
                ////#2-56/2/19, 3rd Floor, Vijaya Towers, near Meridian School,, Ayyappa Society 100ft Road, Madhapur, Ayyappa Society, Chanda Naik Nagar, Madhapur, Hyderabad, Telangana 500081, India
                try {
                    geocoder = new Geocoder(AddAddressActivity.this);
                    addresses = geocoder.getFromLocation(lat, lng, 1);
                    //    save_btn.setEnabled(true);
                    String addLine = addresses.get(0).getAddressLine(0);
                    String city = addresses.get(0).getLocality();
                    etCity.setText(city);
                    String area = addresses.get(0).getSubLocality();
                    etArea.setText(area);
                    String state = addresses.get(0).getAdminArea();
                    etState.setText(state);
                    String country = addresses.get(0).getCountryName();
                    etCountry.setText(country);
                    String postalCode = addresses.get(0).getPostalCode();
                    etPincode.setText(postalCode);
                    String addLineTwo = addresses.get(0).getThoroughfare();
                    // address_two_edt.setText(addLineTwo);
                    String addLineOne = addresses.get(0).getFeatureName();
                    // address_one_edt.setText(addLineOne);
                    if (addr[1].contains(postalCode)) {
                        addr[1] = addr[1].replace(postalCode, " ");
                    }
                    if (addr[1].contains(state)) {
                        addr[1] = addr[1].replace(state, " ");
                    }
                    if (addr[1].contains(country)) {
                        addr[1] = addr[1].replace(country, " ");
                    }
                    if (addr[1].contains(city)) {
                        addr[1] = addr[1].replace(city, " ");
                    }
                    if (addr[1] != null && area != null && addr[1].contains(area)) {
                        addr[1] = addr[1].replace(area, " ");
                    }
                    addr[1] = new LinkedHashSet<String>(Arrays.asList(addr[1].split("\\s"))).toString().replaceAll("[\\[\\],]", "");
                    etAddressline2.setText(addr[1]);
                    etName.setText(username);
                    if (!mobile.equalsIgnoreCase("null"))
                        etPhoneNo.setText(mobile);

                    Log.d("ADDRESSES", addLine + "\n" + city + "\n" + state + "\n" + country + "\n" + postalCode + "\n" + etAddressline1);
                    Log.d("REMAININGADDRESS", addresses.get(0).getSubAdminArea() + "\n" + addresses.get(0).getSubLocality() + "\n" + addresses.get(0).getPremises() + "\n" + addresses.get(0).getSubThoroughfare()
                            + "\n" + addresses.get(0).getThoroughfare());
                    Log.v("FFFFFFF", "//" + address.toString());
                    //address_txt.setText(address);
                    // finaladdress = address.toString();
                    if (mCurrLocationMarker != null) {
                        mCurrLocationMarker.remove();
                    }
                    LatLng latLng = new LatLng(Double.parseDouble(latitude1), Double.parseDouble(longitude1));
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                 /*   markerOptions.title("Current Position");
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                    mCurrLocationMarker = mMap.addMarker(markerOptions);*/

                    //  CircleOptions addCircle = new CircleOptions().center(latLng).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
                    //  mCircle = mMap.addCircle(addCircle);

                    //move map camera
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(19));

                    //stop location updates
                    if (mGoogleApiClient != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("XDIDK", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(AddAddressActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                /*Intent intent =new Intent(CartActivity.this, SmsActivity.class);
                intent.putExtra("link",carBikeItem.getLink());
                intent.putExtra("id",carBikeItem.getId());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);*/
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}

