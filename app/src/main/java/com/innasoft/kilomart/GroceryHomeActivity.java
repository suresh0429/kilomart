package com.innasoft.kilomart;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.innasoft.kilomart.Adapters.SubCatRecyclerViewDataAdapter;
import com.innasoft.kilomart.Adapters.SubCatageoryAdapter;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Helper.Converter;
import com.innasoft.kilomart.Model.HeaderSectionDataModel;
import com.innasoft.kilomart.Model.SingleItemModel;
import com.innasoft.kilomart.Response.HomeResponse;
import com.innasoft.kilomart.Response.ProductResponse;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.kilomart.Storage.Utilities.capitalize;

public class GroceryHomeActivity extends AppCompatActivity {
    public static String MODULE = "grocery";
    AppController appController;
    @BindView(R.id.catRecycler)
    RecyclerView catRecycler;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.simpleSwipeRefreshLayout)
    SwipeRefreshLayout simpleSwipeRefreshLayout;

    ArrayList<HeaderSectionDataModel> allSampleData;


    private SubCatageoryAdapter adapter;
    private SubCatRecyclerViewDataAdapter subCatRecyclerViewDataAdapter;

    private PrefManager pref;
    String userid,tokenValue,deviceId;
    int cartindex;
    String catId,cat_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery_home);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        appController = (AppController) getApplication();

        pref = new PrefManager(getApplicationContext());

        if (getIntent() != null){
            catId = getIntent().getStringExtra("CatId");
            cat_name=getIntent().getStringExtra("Cat_name");

        }

        getSupportActionBar().setTitle(capitalize(cat_name));

        allSampleData = new ArrayList<HeaderSectionDataModel>();


        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userid = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        simpleSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorBlue, R.color.colorPrimary);
        appController = (AppController) getApplicationContext();
        if (appController.isConnection()) {

            prepareSubCatData();
            // cart count
            appController.cartCount(userid,deviceId);
            SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
            cartindex = preferences.getInt("itemCount", 0);
            Log.e("cartindex", "" + cartindex);
            invalidateOptionsMenu();


            simpleSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    prepareSubCatData();

                    simpleSwipeRefreshLayout.setRefreshing(false);
                }
            });

        } else {

            setContentView(R.layout.internet);
            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });


        }


    }

    private void prepareSubCatData() {

        simpleSwipeRefreshLayout.setRefreshing(true);
        Call<HomeResponse> call = RetrofitClient.getInstance().getApi().getHomePageRequest("sub",catId,"");
        call.enqueue(new Callback<HomeResponse>() {
            @Override
            public void onResponse(Call<HomeResponse> call, Response<HomeResponse> response) {
                simpleSwipeRefreshLayout.setRefreshing(false);

                HomeResponse subCatResponse = response.body();

                if (response.isSuccessful()) {

                    if (subCatResponse.getStatus().equalsIgnoreCase("10100")) {

                        if (subCatResponse.getMessage().equalsIgnoreCase("No records found")){

                            Toast.makeText(getApplicationContext(),subCatResponse.getMessage(),Toast.LENGTH_SHORT).show();
                        }else {
                            // subCat Recycler
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
                            catRecycler.setLayoutManager(mLayoutManager);
                            catRecycler.setItemAnimator(new DefaultItemAnimator());
                            catRecycler.setHasFixedSize(true);
                            catRecycler.setNestedScrollingEnabled(false);

                            // subcat Adapter
                            adapter = new SubCatageoryAdapter(getApplicationContext(), subCatResponse.getData(),catId);
                            catRecycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();


                            allSampleData.clear();
                            for (HomeResponse.DataBean subCategoryBean : subCatResponse.getData()){

                                productsData(subCategoryBean.getName(),subCategoryBean.getId(),catId);

                            }
                        }






                    }


                } else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.not_found) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.server_broken) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                        default:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.unknown_error) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                    }
                }


            }

            @Override
            public void onFailure(Call<HomeResponse> call, Throwable t) {
                simpleSwipeRefreshLayout.setRefreshing(false);
                Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

            }
        });


    }

    private void productsData(String title, String subCategory_Id,String category_id){

        final HeaderSectionDataModel dm = new HeaderSectionDataModel();
        dm.setHeaderTitle(title);
        dm.setCategoryId(category_id);
        dm.setSubcategoryId(subCategory_Id);

        final ArrayList<SingleItemModel> singleItem = new ArrayList<SingleItemModel>();

        simpleSwipeRefreshLayout.setRefreshing(true);
        Call<ProductResponse> call = RetrofitClient.getInstance().getApi().getProductList(category_id,subCategory_Id,"0","SINGLE","1",userid,deviceId,"","","LOW_PRICE");
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                simpleSwipeRefreshLayout.setRefreshing(false);

                ProductResponse productResponse1 = response.body();


                if (response.isSuccessful()) {

                    if (productResponse1.getStatus().equalsIgnoreCase("10100")) {


                        for (ProductResponse.DataBean productResponse : productResponse1.getData()){

                            singleItem.add(new SingleItemModel(productResponse.getId(),productResponse.getUrl_name(),
                                    productResponse.getProduct_name(),productResponse.getType(),productResponse.getMain_category_id(),
                                    productResponse.getMain_category_name(),productResponse.getSub_category_id(),productResponse.getSub_category_name(),
                                    productResponse.getChild_category_id(),productResponse.getChild_category_name(),productResponse.getUnit_id(),
                                    productResponse.getUnit_name(),productResponse.getUnit_value(),productResponse.getBrand_id(),
                                    productResponse.getBrand_name(),productResponse.getQty(),productResponse.getMrp_price(),productResponse.getOffer_price(),productResponse.getSelling_price(),productResponse.getAbout(),
                                    productResponse.getMoreinfo(),productResponse.getAvailability(),productResponse.getUser_rating(),productResponse.getFeatures(),productResponse.getPosition(),
                                    productResponse.getSeo_title(),productResponse.getSeo_description(),productResponse.getSeo_keywords(),productResponse.getImages()));
                        }

                        dm.setAllItemsInSection(singleItem);
                        allSampleData.add(dm);

                        // product Recycler
                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        myRecyclerView.setLayoutManager(mLayoutManager1);
                        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
                        myRecyclerView.setHasFixedSize(true);
                        myRecyclerView.setNestedScrollingEnabled(false);

                        // product Adapter
                        subCatRecyclerViewDataAdapter = new SubCatRecyclerViewDataAdapter(getApplicationContext(), allSampleData,singleItem);
                        myRecyclerView.setAdapter(subCatRecyclerViewDataAdapter);
                        subCatRecyclerViewDataAdapter.notifyDataSetChanged();


                    }


                } else {
                    // error case
                    switch (response.code()) {
                        case 404:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.not_found) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                        case 500:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.server_broken) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                        default:
                            Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.unknown_error) + "</font>"), Snackbar.LENGTH_SHORT).show();
                            break;
                    }
                }


            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                simpleSwipeRefreshLayout.setRefreshing(false);
                Snackbar.make(simpleSwipeRefreshLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

            }
        });


    }



    @Override
    protected void onRestart() {

        appController.cartCount(userid,deviceId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onRestart();
    }

    @Override
    protected void onStart() {
        appController.cartCount(userid,deviceId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
        super.onStart();
    }

    ///
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setIcon(Converter.convertLayoutToImage(GroceryHomeActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.action_search:
                Intent intent1 = new Intent(GroceryHomeActivity.this, SearchActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
