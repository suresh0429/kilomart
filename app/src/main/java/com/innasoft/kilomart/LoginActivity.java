package com.innasoft.kilomart;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Reciever.ConnectivityReceiver;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Response.LoginResponse;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;
import com.innasoft.kilomart.Storage.Utilities;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Header;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = "LoginActivity";
    private static final String PREFS_LOCATION = "LOCATION_PREF";
    PrefManager session;
    boolean doubleBackToExitPressedOnce = false;
    Snackbar snackbar;
    @BindView(R.id.imageView)
    AppCompatImageView imageView;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.txtForgotPassword)
    TextView txtForgotPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.txtSignup)
    TextView txtSignup;
    @BindView(R.id.parentLayout)
    LinearLayout parentLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.tilEmail)
    TextInputLayout tilEmail;
    @BindView(R.id.tilPassword)
    TextInputLayout tilPassword;

    String deviceID, fcmToken;

    AppController app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
         app = (AppController) getApplication();

        session = new PrefManager(this);
        if (session.isLoggedIn()) {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finish();
        }

        deviceID = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

        String next = "Not a member ? <font color='#f89113'>SignUp</font> now";
        txtSignup.setText(Html.fromHtml(next));

        etEmail.addTextChangedListener(new MyTextWatcher(etEmail));
        etPassword.addTextChangedListener(new MyTextWatcher(etPassword));


        // fcm Token
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmToken = instanceIdResult.getToken();
                Log.e("TOKEN", "" + fcmToken);
            }
        });

    }


    /* Validating form
     */
    private void submitForm() {

        if ((!isValidEmail(etEmail.getText().toString().trim()))) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        userLogin();

    }

    // retrofit login
    private void userLogin() {


        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();


        progressBar.setVisibility(View.VISIBLE);
        btnLogin.setEnabled(false);

        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(email, password, deviceID);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {


                progressBar.setVisibility(View.GONE);

                btnLogin.setEnabled(true);

                LoginResponse loginResponse = response.body();


                if (loginResponse.getStatus().equals("10100")) {

                    if (loginResponse.getData().getMobile_verify_status().equalsIgnoreCase("0")){

                        Intent intent = new Intent(LoginActivity.this, VerifyOtpActivity.class);
                        intent.putExtra("MobileNumber",loginResponse.getData().getMobile());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                        sendOtp(loginResponse.getData().getMobile());
                    }
                    else {

                        Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();

                        session.createLogin(loginResponse.getData().getUser_id(),
                                loginResponse.getData().getUser_name(),
                                loginResponse.getData().getEmail(),
                                loginResponse.getData().getMobile(),
                                null, deviceID,
                                loginResponse.getData().getJwt(),
                                loginResponse.getData().getGender());

                        // location preferences
                        setDefaults();

                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                        updateFcmToken(loginResponse.getData().getJwt(), loginResponse.getData().getUser_id());

                    }



                } else if (loginResponse.getStatus().equals("10200")) {
                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (loginResponse.getStatus().equals("10300")) {
                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (loginResponse.getStatus().equals("10400")) {
                    Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else {
                    int color = Color.RED;
                    snackBar(loginResponse.getMessage(), color);

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btnLogin.setEnabled(true);
            }
        });
    }


    private void sendOtp(String mobile){
        Call<BaseResponse> call1=RetrofitClient.getInstance().getApi().ResendOtpRequest(mobile);
        call1.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful());
                BaseResponse resendOtpResponse=response.body();

                if (resendOtpResponse.getStatus().equals("10100")){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (resendOtpResponse.getStatus().equals("10200")){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (resendOtpResponse.getStatus().equals("10300")){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (resendOtpResponse.getStatus().equals("10400")){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, resendOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();


            }
        });
    }

    // update Fcm Token
    private void updateFcmToken(String jwt, String user_id) {

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().updateFcmTocken(jwt, user_id, fcmToken);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {


                progressBar.setVisibility(View.GONE);


                BaseResponse loginResponse = response.body();


                if (loginResponse.getStatus().equals("10100")) {

                    Log.d(TAG, "onResponse: " + loginResponse.getMessage());


                } else if (loginResponse.getStatus().equals("10200")) {
                    Log.d(TAG, "onResponse: " + loginResponse.getMessage());
                } else if (loginResponse.getStatus().equals("10300")) {
                    Log.d(TAG, "onResponse: " + loginResponse.getMessage());
                } else if (loginResponse.getStatus().equals("10400")) {
                    Log.d(TAG, "onResponse: " + loginResponse.getMessage());
                } else {
                    int color = Color.RED;
                    snackBar(loginResponse.getMessage(), color);

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btnLogin.setEnabled(true);
            }
        });
    }


    public void setDefaults() {
        SharedPreferences preferences = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, true);
        editor.putString(Utilities.KEY_AREA,"");
        editor.putString(Utilities.KEY_PINCODE,"");
        editor.putString(Utilities.KEY_SHIPPINGCHARGES,"");
        editor.apply();
    }


    @OnClick({R.id.txtForgotPassword, R.id.btnLogin, R.id.txtSignup})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtForgotPassword:
                Intent intent2 = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent2);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.btnLogin:

                // Method to manually check connection status
                boolean isConnected = app.isConnection();

                if (isConnected) {
                    submitForm();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;

                    snackBar(message, color);
                    //showSnack(isConnected);
                }

                break;
            case R.id.txtSignup:
                Intent intent1 = new Intent(LoginActivity.this, RegisterActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }

    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    // validate phone
//    private boolean isValidPhoneNumber(String mobile) {
//        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
//        Matcher matcher = pattern.matcher(mobile);
//
//        if (mobile.isEmpty()) {
//            tilPhone.setError("Phone no is required");
//            requestFocus(etPhone);
//            return false;
//        } else if (!matcher.matches()) {
//            tilPhone.setError("Enter a valid mobile");
//            requestFocus(etPhone);
//            return false;
//        } else {
//            tilPhone.setErrorEnabled(false);
//        }
//
//        return matcher.matches();
//    }

    // validate password
    private boolean validatePassword() {
        if (etPassword.getText().toString().trim().isEmpty()) {
            tilPassword.setError("Password required");
            requestFocus(etPassword);
            return false;
        } else if (etPassword.length() < 6) {
            tilPassword.setError("Password should be atleast 6 character long");
            requestFocus(etPassword);
            return false;
        } else {
            tilPassword.setErrorEnabled(false);
        }

        return true;
    }

    // request focus
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etPhone:
                    isValidEmail(etEmail.getText().toString().trim());
                    break;
                case R.id.etPassword:
                    validatePassword();
                    break;
            }
        }
    }

    private boolean isValidEmail(String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        if (email.isEmpty()) {
            tilEmail.setError("Email is required");
            requestFocus(etEmail);
            return false;
        } else if (!matcher.matches()) {
            tilEmail.setError("Enter a valid email");
            requestFocus(etEmail);
            return false;
        } else {
            tilEmail.setErrorEnabled(false);
        }


        return matcher.matches();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            moveTaskToBack(true);
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);


    }
}