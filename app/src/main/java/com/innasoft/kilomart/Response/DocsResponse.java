package com.innasoft.kilomart.Response;

import java.util.List;

public class DocsResponse {


    /**
     * status : 1
     * docs : [{"id":"1","category_name":"rentals","file_path":"https://pickany24x7.com/image/terms_and_onditions/961509251_TERMS+AND+CONDITIONS+OF+CAR+RENTALS.pdf","file_type":"terms_and_onditions","status":"1"},{"id":"2","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/271257675_Final+Return+Policy-converted.pdf","file_type":"return_policies","status":"1"},{"id":"3","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/414115476_Privacy-%26-Policy-converted.pdf","file_type":"privacy_policies","status":"1"},{"id":"4","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/398144600_Terms+%26+Conditions-converted.pdf","file_type":"terms_and_onditions","status":"1"},{"id":"5","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/693942752_FAQS.pdf","file_type":"faqs","status":"1"},{"id":"6","category_name":"all","file_path":"https://pickany24x7.com/image/terms_and_onditions/502322564_ABOUTUS.pdf","file_type":"aboutus","status":"1"}]
     * message : Get Documents
     */

    private int status;
    private String message;
    private List<DocsBean> docs;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DocsBean> getDocs() {
        return docs;
    }

    public void setDocs(List<DocsBean> docs) {
        this.docs = docs;
    }

    public static class DocsBean {
        /**
         * id : 1
         * category_name : rentals
         * file_path : https://pickany24x7.com/image/terms_and_onditions/961509251_TERMS+AND+CONDITIONS+OF+CAR+RENTALS.pdf
         * file_type : terms_and_onditions
         * status : 1
         */

        private String id;
        private String category_name;
        private String file_path;
        private String file_type;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getFile_path() {
            return file_path;
        }

        public void setFile_path(String file_path) {
            this.file_path = file_path;
        }

        public String getFile_type() {
            return file_type;
        }

        public void setFile_type(String file_type) {
            this.file_type = file_type;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
