package com.innasoft.kilomart.Adapters;

/**
 * Created by pratap.kesaboyina on 24-12-2014.
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.innasoft.kilomart.Model.SingleItemModel;
import com.innasoft.kilomart.ProductDetailsActivity;
import com.innasoft.kilomart.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.innasoft.kilomart.Apis.RetrofitClient.PRODUCT_IMAGE_BASE_URL;
import static com.innasoft.kilomart.Apis.RetrofitClient.PRODUCT_IMAGE_BASE_URL2;
import static com.innasoft.kilomart.Storage.Utilities.capitalize;

public class ProductSectionListDataAdapter extends RecyclerView.Adapter<ProductSectionListDataAdapter.SingleItemRowHolder> {


    private ArrayList<SingleItemModel> itemsList;
    private Context mContext;

    public ProductSectionListDataAdapter(Context context, ArrayList<SingleItemModel> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        final SingleItemModel singleItem = itemsList.get(i);

        if (singleItem.getAvailability().equalsIgnoreCase("1")){
            holder.shadowImageView.setVisibility(View.VISIBLE);
        }else {
            holder.shadowImageView.setVisibility(View.GONE);

        }


        holder.product_unit.setText("" +singleItem.getUnit_value()+singleItem.getUnit_name());
        holder.txtproductPrice.setText("\u20B9" + singleItem.getMrp_price());
        holder.txtproductPrice.setPaintFlags(holder.txtproductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtproductPrice.setTextColor(Color.RED);

        holder.txtDiscountPrice.setText("\u20B9" + singleItem.getSelling_price());
        Glide.with(mContext).load(PRODUCT_IMAGE_BASE_URL2+ singleItem.getImages().get(0)).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(holder.thumbnail);
        holder.txtproductName.setText(capitalize(singleItem.getProduct_name()));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productId",singleItem.getId());
                intent.putExtra("productName",singleItem.getProduct_name());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

       /* Glide.with(mContext)
                .load(feedItem.getImageURL())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .error(R.drawable.bg)
                .into(feedListRowHolder.thumbView);*/
    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.txtproductName)
        TextView txtproductName;
        @BindView(R.id.product_unit)
        TextView product_unit;
        @BindView(R.id.txtproductPrice)
        TextView txtproductPrice;
        @BindView(R.id.txtDiscountPrice)
        TextView txtDiscountPrice;
        @BindView(R.id.txtDiscountTag)
        TextView txtDiscountTag;
        @BindView(R.id.shadowImageView)
        LinearLayout shadowImageView;
        @BindView(R.id.tagLayout)
        LinearLayout tagLayout;
        @BindView(R.id.parentLayout)
        CardView parentLayout;
        public SingleItemRowHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);

           /* this.tvTitle = (TextView) view.findViewById(R.id.txtproductName);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();

                }
            });*/


        }

    }

}