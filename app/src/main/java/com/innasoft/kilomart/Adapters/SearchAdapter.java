package com.innasoft.kilomart.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.innasoft.kilomart.ProductDetailsActivity;
import com.innasoft.kilomart.R;
import com.innasoft.kilomart.Response.ProductResponse;
import com.innasoft.kilomart.Response.SearchResponse;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import static com.innasoft.kilomart.Apis.RetrofitClient.PRODUCT_IMAGE_BASE_URL2;
import static com.innasoft.kilomart.Storage.Utilities.capitalize;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder>{
    private Context mContext;
    private List<ProductResponse.DataBean> homeList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView thumbnail;
        public TextView txtPrice, txtName, txtDiscountPrice, txtDiscountTag,product_unit;
        //  public RatingBar ratingBar;
        public LinearLayout  tagLayout;
        public CardView linearLayout;

        public MyViewHolder(View view) {
            super(view);

            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            txtPrice = (TextView) view.findViewById(R.id.txtproductPrice);
            product_unit = (TextView) view.findViewById(R.id.product_unit);
            txtName = (TextView) view.findViewById(R.id.txtproductName);
            txtDiscountPrice = (TextView) view.findViewById(R.id.txtDiscountPrice);
            linearLayout=(CardView)view.findViewById(R.id.parentLayout);
            tagLayout = (LinearLayout)view.findViewById(R.id.tagLayout);
            txtDiscountTag = (TextView)view.findViewById(R.id.txtDiscountTag);


        }
    }


    public SearchAdapter(Context mContext, List<ProductResponse.DataBean> homekitchenList) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.products_item_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ProductResponse.DataBean home = homeList.get(position);


        // loading album cover using Glide library
        Glide.with(mContext).load(PRODUCT_IMAGE_BASE_URL2+home.getImages().get(0)).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(holder.thumbnail);
        Log.d("ADAPTERIMASGE", "onBindViewHolder: "+PRODUCT_IMAGE_BASE_URL2+home.getImages().get(0));
        holder.txtName.setText(capitalize(home.getProduct_name()));
        holder.txtDiscountPrice.setText("\u20B9"+home.getSelling_price());

        holder.txtPrice.setText("\u20B9" + home.getMrp_price());
        holder.txtPrice.setPaintFlags(holder.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txtPrice.setTextColor(Color.RED);

        holder.product_unit.setText("" +home.getUnit_value()+home.getUnit_name());

        //  holder.ratingBar.setRating(Float.parseFloat(home.getRating()));
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //                    Activity activity = (Activity) mContext;


                Intent intent = new Intent(mContext, ProductDetailsActivity.class);
                intent.putExtra("productId",home.getId());
                intent.putExtra("productName",home.getProduct_name());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                // activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);


            }});


    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }



}
