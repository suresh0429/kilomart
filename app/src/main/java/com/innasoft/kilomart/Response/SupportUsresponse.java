package com.innasoft.kilomart.Response;

public class SupportUsresponse {


    /**
     * status : 1
     * message : Success
     */

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
