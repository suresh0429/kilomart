package com.innasoft.kilomart.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.innasoft.kilomart.GroceryHomeActivity;
import com.innasoft.kilomart.ProductListActivity;
import com.innasoft.kilomart.R;
import com.innasoft.kilomart.Response.HomeResponse;


import java.util.List;

import static com.innasoft.kilomart.Apis.RetrofitClient.IMAGE_BASE_URL;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {
    private Context mContext;
    private List<HomeResponse.DataBean> homeList;
    // ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView thumbNail;
        TextView txtTitle, txtPending;
        ProgressBar progressBar;
        //  LinearLayout tagLayout;
        CardView cardView;


        public MyViewHolder(View view) {
            super(view);

            thumbNail = (ImageView) view.findViewById(R.id.thumbnail);
            txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            // tagLayout = (LinearLayout) view.findViewById(R.id.tagLayout);
           // txtPending = (TextView) view.findViewById(R.id.txtPending);
            cardView = (CardView) view.findViewById(R.id.cardView);

        }
    }

    public HomeAdapter(Context mContext, List<HomeResponse.DataBean> homekitchenList) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final HomeResponse.DataBean home = homeList.get(position);

        holder.txtTitle.setText(home.getName());
        Glide.with(mContext).load(IMAGE_BASE_URL+home.getImage()).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(holder.thumbNail);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (home.getId().equalsIgnoreCase("5") || home.getName().equalsIgnoreCase("Baby care")){

                    Intent intent1 = new Intent(mContext, ProductListActivity.class);
                    intent1.putExtra("catId", home.getId());
                    intent1.putExtra("title", home.getName());
                    intent1.putExtra("subcatId", "");
                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent1);
                }
                else {
                    Intent intent = new Intent(mContext, GroceryHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("CatId", home.getId());
                    intent.putExtra("Cat_name",home.getName());
                    mContext.startActivity(intent);
                }

            }

        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


/*
    @Override
    public int getItemViewType(int position) {
        if(position% 2 == 1) {
            return 2;
        }else{
            return 3;
        }
    }
*/
}
