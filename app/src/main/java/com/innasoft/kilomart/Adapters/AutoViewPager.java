package com.innasoft.kilomart.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.demono.adapter.InfinitePagerAdapter;
import com.innasoft.kilomart.R;
import com.innasoft.kilomart.Response.BannersResponse;
import com.innasoft.kilomart.Response.HomeResponse;

import java.util.List;

import static com.innasoft.kilomart.Apis.RetrofitClient.IMAGE_BASE_URL;

public class AutoViewPager extends InfinitePagerAdapter {

    private List<BannersResponse.DataBean> data;
    private Context context;

    public AutoViewPager(List<BannersResponse.DataBean> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup container) {
        BannersResponse.DataBean homeBannersBean = data.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.banner_card, container, false);
        }
        // Lookup view for data population
        TextView tvDiscription = (TextView) convertView.findViewById(R.id.txtDescription);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.imageView);
        // Populate the data into the template view using the data object
        tvDiscription.setText(homeBannersBean.getHighlighted_text());
        Glide.with(context).load(IMAGE_BASE_URL+ homeBannersBean.getImage()).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(imageView);
        // Return the completed view to render on screen
        return convertView;
    }
}
