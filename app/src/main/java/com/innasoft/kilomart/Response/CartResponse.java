package com.innasoft.kilomart.Response;

import java.util.List;

public class CartResponse {


    /**
     * status : 10100
     * message : Data fetch successfully
     * data : {"records":[{"cart_id":"4","product_id":"5","product_name":"Peanuts","selling_price":"60.00","mrp_price":"60.00","purchase_quantity":"1","unit_id":"1","unit_name":"Kg","unit_value":"4","brand_id":"1","brand_name":"KILOMART","images":"3542a-peanuts.png","net_amount":60,"gross_amount":60},{"cart_id":"2","product_id":"6","product_name":"Idly rava","selling_price":"45.00","mrp_price":"50.00","purchase_quantity":"1","unit_id":"1","unit_name":"Kg","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"959dd-idly-rava.png","net_amount":45,"gross_amount":50}],"final_gross_amount":"110.00","final_net_amount":"105.00","cart_discount":"0.00","coupon_discount":"0.00","voucher_discount":"0.00","final_discount":"5.00","payable_amount":"105.00"}
     */

    private String status;
    private String message;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * records : [{"cart_id":"4","product_id":"5","product_name":"Peanuts","selling_price":"60.00","mrp_price":"60.00","purchase_quantity":"1","unit_id":"1","unit_name":"Kg","unit_value":"4","brand_id":"1","brand_name":"KILOMART","images":"3542a-peanuts.png","net_amount":60,"gross_amount":60},{"cart_id":"2","product_id":"6","product_name":"Idly rava","selling_price":"45.00","mrp_price":"50.00","purchase_quantity":"1","unit_id":"1","unit_name":"Kg","unit_value":"1","brand_id":"1","brand_name":"KILOMART","images":"959dd-idly-rava.png","net_amount":45,"gross_amount":50}]
         * final_gross_amount : 110.00
         * final_net_amount : 105.00
         * cart_discount : 0.00
         * coupon_discount : 0.00
         * voucher_discount : 0.00
         * final_discount : 5.00
         * payable_amount : 105.00
         */

        private String final_gross_amount;
        private String final_net_amount;
        private String cart_discount;
        private String coupon_discount;
        private String voucher_discount;
        private String final_discount;
        private String payable_amount;
        private List<RecordsBean> records;

        public String getFinal_gross_amount() {
            return final_gross_amount;
        }

        public void setFinal_gross_amount(String final_gross_amount) {
            this.final_gross_amount = final_gross_amount;
        }

        public String getFinal_net_amount() {
            return final_net_amount;
        }

        public void setFinal_net_amount(String final_net_amount) {
            this.final_net_amount = final_net_amount;
        }

        public String getCart_discount() {
            return cart_discount;
        }

        public void setCart_discount(String cart_discount) {
            this.cart_discount = cart_discount;
        }

        public String getCoupon_discount() {
            return coupon_discount;
        }

        public void setCoupon_discount(String coupon_discount) {
            this.coupon_discount = coupon_discount;
        }

        public String getVoucher_discount() {
            return voucher_discount;
        }

        public void setVoucher_discount(String voucher_discount) {
            this.voucher_discount = voucher_discount;
        }

        public String getFinal_discount() {
            return final_discount;
        }

        public void setFinal_discount(String final_discount) {
            this.final_discount = final_discount;
        }

        public String getPayable_amount() {
            return payable_amount;
        }

        public void setPayable_amount(String payable_amount) {
            this.payable_amount = payable_amount;
        }

        public List<RecordsBean> getRecords() {
            return records;
        }

        public void setRecords(List<RecordsBean> records) {
            this.records = records;
        }

        public static class RecordsBean {
            /**
             * cart_id : 4
             * product_id : 5
             * product_name : Peanuts
             * selling_price : 60.00
             * mrp_price : 60.00
             * purchase_quantity : 1
             * unit_id : 1
             * unit_name : Kg
             * unit_value : 4
             * brand_id : 1
             * brand_name : KILOMART
             * images : 3542a-peanuts.png
             * net_amount : 60
             * gross_amount : 60
             */

            private String cart_id;
            private String product_id;
            private String product_name;
            private String selling_price;
            private String mrp_price;
            private String purchase_quantity;
            private String unit_id;
            private String unit_name;
            private String unit_value;
            private String brand_id;
            private String brand_name;
            private String images;
            private int net_amount;
            private int gross_amount;

            public String getCart_id() {
                return cart_id;
            }

            public void setCart_id(String cart_id) {
                this.cart_id = cart_id;
            }

            public String getProduct_id() {
                return product_id;
            }

            public void setProduct_id(String product_id) {
                this.product_id = product_id;
            }

            public String getProduct_name() {
                return product_name;
            }

            public void setProduct_name(String product_name) {
                this.product_name = product_name;
            }

            public String getSelling_price() {
                return selling_price;
            }

            public void setSelling_price(String selling_price) {
                this.selling_price = selling_price;
            }

            public String getMrp_price() {
                return mrp_price;
            }

            public void setMrp_price(String mrp_price) {
                this.mrp_price = mrp_price;
            }

            public String getPurchase_quantity() {
                return purchase_quantity;
            }

            public void setPurchase_quantity(String purchase_quantity) {
                this.purchase_quantity = purchase_quantity;
            }

            public String getUnit_id() {
                return unit_id;
            }

            public void setUnit_id(String unit_id) {
                this.unit_id = unit_id;
            }

            public String getUnit_name() {
                return unit_name;
            }

            public void setUnit_name(String unit_name) {
                this.unit_name = unit_name;
            }

            public String getUnit_value() {
                return unit_value;
            }

            public void setUnit_value(String unit_value) {
                this.unit_value = unit_value;
            }

            public String getBrand_id() {
                return brand_id;
            }

            public void setBrand_id(String brand_id) {
                this.brand_id = brand_id;
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public int getNet_amount() {
                return net_amount;
            }

            public void setNet_amount(int net_amount) {
                this.net_amount = net_amount;
            }

            public int getGross_amount() {
                return gross_amount;
            }

            public void setGross_amount(int gross_amount) {
                this.gross_amount = gross_amount;
            }
        }
    }
}
