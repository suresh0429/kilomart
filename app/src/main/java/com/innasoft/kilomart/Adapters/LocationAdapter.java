package com.innasoft.kilomart.Adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.innasoft.kilomart.HomeActivity;
import com.innasoft.kilomart.R;
import com.innasoft.kilomart.Response.LocationsResponse;
import com.innasoft.kilomart.Storage.Utilities;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.innasoft.kilomart.Storage.Utilities.capitalize;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.MyViewHolder> {


    private Context mContext;
    private List<LocationsResponse.DataBean> homeList;
    private AlertDialog alertDialog;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtLocation)
        TextView txtLocation;
        @BindView(R.id.parentLayout)
        LinearLayout parentLayout;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }

    public LocationAdapter(Context mContext, List<LocationsResponse.DataBean> homekitchenList, AlertDialog alertDialog) {
        this.mContext = mContext;
        this.homeList = homekitchenList;
        this.alertDialog = alertDialog;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.location_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final LocationsResponse.DataBean home = homeList.get(position);

        holder.txtLocation.setText(capitalize(home.getArea()));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String area=home.getArea();
                Intent intent = new Intent(mContext, HomeActivity.class);
//                intent.putExtra("loc_Area",area);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);

                alertDialog.dismiss();

                // update location preferences
                SharedPreferences preferences = mContext.getSharedPreferences(Utilities.PREFS_LOCATION, 0);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
                editor.putString(Utilities.KEY_AREA,home.getArea());
                editor.putString(Utilities.KEY_PINCODE,home.getPincode());
                editor.putString(Utilities.KEY_SHIPPINGCHARGES,home.getShipping_charges());
                editor.apply();

            }

        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return homeList.size();
    }


}
