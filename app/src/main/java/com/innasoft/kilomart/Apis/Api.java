package com.innasoft.kilomart.Apis;



import com.innasoft.kilomart.Response.AddressResponse;
import com.innasoft.kilomart.Response.AppVersionResponse;
import com.innasoft.kilomart.Response.BannersResponse;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Response.CartCountResponse;
import com.innasoft.kilomart.Response.CartResponse;
import com.innasoft.kilomart.Response.CheckoutPostResponse;
import com.innasoft.kilomart.Response.CheckoutResponse;
import com.innasoft.kilomart.Response.LocationsResponse;
import com.innasoft.kilomart.Response.NotificationReadResponse;
import com.innasoft.kilomart.Response.NotificationsResponse;
import com.innasoft.kilomart.Response.OrderListResponse;
import com.innasoft.kilomart.Response.ProductResponse;
import com.innasoft.kilomart.Response.ProductSingleResponse;
import com.innasoft.kilomart.Response.HomeResponse;
import com.innasoft.kilomart.Response.LoginResponse;
import com.innasoft.kilomart.Response.MyOrdersResponse;
import com.innasoft.kilomart.Response.PayuMoneyPutResponse;
import com.innasoft.kilomart.Response.SearchResponse;
import com.innasoft.kilomart.Response.SupportUsresponse;
import com.innasoft.kilomart.Response.VerifyOtpResponse;
import com.innasoft.kilomart.Response.WishListDeleteresponse;
import com.innasoft.kilomart.Response.WishListPostResponse;
import com.innasoft.kilomart.Response.WishlistResponse;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @FormUrlEncoded
    @POST("users/verify-login")
    Call<LoginResponse> userLogin(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("browser_id") String brower_id);

    @FormUrlEncoded
    @POST("users/registration")
    Call<BaseResponse> userRegistrationRequest(@Field("name") String name,@Field("email") String email,
                                                   @Field("mobile") String mobile,@Field("conf_pwd") String conf_pwd);


    @FormUrlEncoded
    @POST("users/verify-otp")
    Call<VerifyOtpResponse> userOtpRequest(@Field("mobile") String mobile,
                                           @Field("otp") String otp,
                                           @Field("browser_id") String browser_id);

    @FormUrlEncoded
    @POST("users/resend-otp")
    Call<BaseResponse> ResendOtpRequest(@Field("mobile") String mobile);



    @FormUrlEncoded
    @POST("users/forgot-password")
    Call<BaseResponse> userForgotPassword(@Field("mobile") String mobile);


    @FormUrlEncoded
    @POST("users/verify-otp-forgot-password")
    Call<BaseResponse> userVerifyForgotPassword(@Field("mobile") String mobile,
                                                @Field("otp") String otp,
                                                @Field("browser_id") String browser_id,
                                                @Field("new_pwd") String new_pwd);


    @FormUrlEncoded
    @POST("user/{id}/logout")
    Call<BaseResponse> userLogout(@Header ("Authorization-Basic") String tokenValue,@Path("id") String id, @Field("browser_id") String browser_id);

    @FormUrlEncoded
    @POST("user/{id}/change-password")
    Call<BaseResponse> userChangePassword(@Header ("Authorization-Basic") String tokenValue,@Path("id") String id, @Field("old_pwd") String old_pwd,@Field("new_pwd") String new_pwd);

    @FormUrlEncoded
    @POST("user/{id}/profile")
    Call<BaseResponse> updateProfile(@Header ("Authorization-Basic") String tokenValue,@Path("id") String id,
                                          @Field("name") String name,@Field("email") String email,
                                          @Field("mobile") String mobile,@Field("gender") String gender);



    @GET("banners")
    Call<BannersResponse> getHomeBanners();


    @GET("locations")
    Call<LocationsResponse> getLocations();



    @GET("categories")
    Call<HomeResponse> getHomePageRequest(@Query("type") String type,@Query("main_category") String main_category,@Query("sub_category") String sub_category);



    @GET("products")
    Call<ProductResponse> getPopularProduct(@Query("type") String type,
                                           @Query("browser_id") String browser_id,
                                           @Query("sort_by") String sort_by
                                           //@Query("search") String search
    );



    @GET("products")
    Call<ProductResponse> getProductSearch(@Query("type") String type,
                                         /*@Query("user_id") String user_id,*/
                                         @Query("browser_id") String browser_id,
                                         @Query("search") String search

    );


    @GET("products")
    Call<ProductResponse> getProductList(@Query("main_category") String main_category,
                                         @Query("sub_category") String sub_category,
                                         @Query("child_category") String child_category,
                                         @Query("type") String type,
                                         @Query("page") String page,
                                         @Query("user_id") String user_id,
                                         @Query("browser_id") String browser_id,
                                         @Query("search") String search,
                                         @Query("brand_id") String brand_id,
                                         @Query("sort_by") String sort_by
                                          );





    @GET("product/{id}")
    Call<ProductSingleResponse> getProductdetails(@Path("id") String productId, @Query("browser_id") String browser_id);

    @FormUrlEncoded
    @POST("cart")
    Call<BaseResponse> addtoCart(@Field("product_id") String product_id, @Field("browser_id") String browser_id,
                                                 @Field("user_id") String user_id, @Field("purchase_quantity") String purchase_quantity);


    @GET("cart")
    Call<CartResponse> cartItemsList(@Query("user_id") String userId,@Query("browser_id") String browser_id);


    @FormUrlEncoded
    @POST("cart/quantity")
    Call<BaseResponse> updateQuantity(@Field("cart_id") String cart_id,
                                                @Field("purchase_quantity") String purchase_quantity);

    @FormUrlEncoded
    @POST("cart/delete")
    Call<BaseResponse> deleteCart(@Field("user_id") String user_id,@Field("cart_id") String cart_id,@Field("browser_id") String browser_id);



    @GET("cart/count")
    Call<CartCountResponse> getCartCount(@Query("user_id") String userId, @Query("browser_id") String  browser_id);


    @GET("user/{id}/addresses")
    Call<AddressResponse> getAddress(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId);


    @FormUrlEncoded
    @POST("user/{id}/address")
    Call<BaseResponse> userAddress(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId,
                                          @Field("name") String name, @Field("address_line1") String address_line1,
                                          @Field("address_line2") String address_line2, @Field("area") String area,
                                          @Field("city") String city, @Field("state") String state,
                                          @Field("pincode") String pincode, @Field("contact_no") String contact_no,
                                          @Field("alternate_contact_no") String alternate_contact_no,@Field("latitude") String latitude,
                                          @Field("longitude") String longitude,@Field("is_default") String is_default
                                          );


    @FormUrlEncoded
    @POST("user/{id}/address/{id1}")
    Call<BaseResponse> updateuserAddress(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId,@Path("id1") String addressid,
                                   @Field("name") String name, @Field("address_line1") String address_line1,
                                   @Field("address_line2") String address_line2, @Field("area") String area,
                                   @Field("city") String city, @Field("state") String state,
                                   @Field("pincode") String pincode, @Field("contact_no") String contact_no,
                                   @Field("alternate_contact_no") String alternate_contact_no,@Field("latitude") String latitude,
                                   @Field("longitude") String longitude,@Field("is_default") String is_default
    );



    @DELETE("user/{id}/address/{id1}")
    Call<BaseResponse> deleteuserAddress(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId,@Path("id1") String addressid);



    @GET("checkout")
    Call<CheckoutResponse> checkOutStatus(@Header ("Authorization-Basic") String tokenValue,@Query("user_id") String user_id,@Query("address_id") String address_id);


    @FormUrlEncoded
    @POST("checkout")
    Call<CheckoutPostResponse> checkoutPost(@Header ("Authorization-Basic") String tokenValue, @Field("user_id") String user_id, @Field("address_id") String address_id, @Field("code") String code,
                                                     @Field("payment_gateway_id") String payment_gateway_id, @Field("platform") String platform);


    @GET("user/{id}/orders")
    Call<MyOrdersResponse> MyOrders(@Header ("Authorization-Basic") String tokenValue,@Path("id") String userId);

    @GET("user/{id}/orders/{id1}")
    Call<OrderListResponse> getOrderList(@Header ("Authorization-Basic") String tokenValue, @Path("id") String userId, @Path("id1") String orderId);

    @GET("user/{id}/notification")
    Call<NotificationsResponse> Notifications(@Header ("Authorization-Basic") String tokenValue, @Path("id") String userId);

    @PUT("user/{id}/notification/{id1}/read")
    Call<NotificationReadResponse> NotificationRead(@Header ("Authorization-Basic") String tokenValue, @Path("id") String userId, @Path("id1") String notificationId);


    @FormUrlEncoded
    @PUT("fcm-token")
    Call<BaseResponse> updateFcmTocken(@Header ("Authorization-Basic") String tokenValue, @Field("user_id") String user_id, @Field("fcm_token") String fcm_token);


    @GET("app/version")
    Call<AppVersionResponse> CheckAppUpdate(@Header ("Authorization-Basic") String tokenValue);



    @GET("wishList")
    Call<WishlistResponse> getWhishlisList(@Query("user_id") String userId);



    @DELETE("wishList_items/{id}")
    Call<WishListDeleteresponse> deleteWishList(@Path("id") String id);


    @FormUrlEncoded
    @POST("wishList")
    Call<WishListPostResponse> addWishList(@Field("module") String module, @Field("user_id") String userId,
                                           @Field("item_id") String item_id, @Field("add_type") String add_type);

}
