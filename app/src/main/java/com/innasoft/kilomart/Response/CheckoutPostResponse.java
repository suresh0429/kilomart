package com.innasoft.kilomart.Response;

public class CheckoutPostResponse {


    /**
     * status : 10100
     * message : Thank you! Your order has been placed successfully. Reference ID: KM2019091601002185.
     * order_id : 3
     */

    private String status;
    private String message;
    private int order_id;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }
}
