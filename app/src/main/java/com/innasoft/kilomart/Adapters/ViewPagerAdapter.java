package com.innasoft.kilomart.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.innasoft.kilomart.R;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {
    private static final String TAG = "ViewPagerAdapter";
    private List<String> images;
    private LayoutInflater inflater;
    private Context context;
    private String availability;

    public ViewPagerAdapter(Context context, List<String> images, String availability) {
        this.context = context;
        this.images=images;
        this.availability=availability;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.slide_card, view, false);


        ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.thumbnail);
        LinearLayout shadowImageView = (LinearLayout) myImageLayout.findViewById(R.id.shadowImageView);

        if (availability.equalsIgnoreCase("1")){
            shadowImageView.setVisibility(View.VISIBLE);
        }else {
            shadowImageView.setVisibility(View.GONE);

        }

        Log.d(TAG, "instantiateItem: "+availability);

        try {
            Glide.with(context).load(images.get(position)).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.placeholder).into(myImage);
        } catch (Exception e) {
            e.printStackTrace();
        }

       /* myImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductImageActivity.class);
                intent.putStringArrayListExtra("ViewPager", (ArrayList<String>) images);
                intent.putExtra("productImageName","Images");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context. startActivity(intent);
            }
        });*/


        view.addView(myImageLayout, position);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}