package com.innasoft.kilomart.Singleton;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;


import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Reciever.ConnectivityReceiver;
import com.innasoft.kilomart.Response.CartCountResponse;
import com.innasoft.kilomart.Storage.PrefManager;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppController extends Application {

    public static final String TAG = AppController.class
            .getSimpleName();


    private static AppController mInstance;

    private PrefManager pref;
    private static final String PREFS_NAME = "CARTCOUNT";




    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
       /* HashMap<String, String> profile = pref.getUserDetails();
        String userId=profile.get("id");*/

       // appEnvironment = AppEnvironment.SANDBOX;




    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }





    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    // check internet connection
    public boolean isConnection(){

        ConnectivityManager connectivityManager=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo=connectivityManager.getActiveNetworkInfo();

        return networkInfo !=null && networkInfo.isConnected();
    }





    public void cartCount(final String userId,final String deviceId) {

        Call<CartCountResponse> call = RetrofitClient.getInstance().getApi().getCartCount(userId,deviceId);

        call.enqueue(new Callback<CartCountResponse>() {
            @Override
            public void onResponse(Call<CartCountResponse> call, Response<CartCountResponse> response) {

                if (response.isSuccessful()){

                    CartCountResponse cartCountResponse = response.body();

                    if (cartCountResponse.getStatus().equalsIgnoreCase("10100")) {

                        int cartindex =cartCountResponse.getData();
                        Log.e("CART_INDEX",""+cartindex);




                        SharedPreferences preferences = getSharedPreferences(PREFS_NAME, 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("itemCount", cartindex);
                        editor.apply();


                    }else if (cartCountResponse.getStatus().equalsIgnoreCase("10200")){
                        Toast.makeText(getApplicationContext(),cartCountResponse.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                    else{

                        SharedPreferences preferences =getSharedPreferences(PREFS_NAME,0);
                        SharedPreferences.Editor editor =preferences.edit();
                        editor.putInt("itemCount",0);
                        editor.apply();
                    }

                }


            }

            @Override
            public void onFailure(Call<CartCountResponse> call, Throwable t) {
                SharedPreferences preferences =getSharedPreferences(PREFS_NAME,0);
                SharedPreferences.Editor editor =preferences.edit();
                editor.putInt("itemCount",0);
                editor.apply();
            }
        });

    }







}
