package com.innasoft.kilomart.Model;

public class MyOrderItem {

    public boolean checked;
    String product_name;
    String id;
    String user_id;
    String order_id;
    String category;
    String cart_type;
    String item_id;
    String quantity;
    String unit_price_incl_tax;
    String unit_price_exld_tax;
    String tax_rate;
    String tax_amt;
    String discount;
    String total_price;
    String color;
    String delivery_time;
    String order_status;
    String reason_for_cancel_or_return;
    String photo_cake_image;
    String delivery_charges;
    String date_of_order;
    String image_path;


    public MyOrderItem(boolean checked, String product_name, String id, String user_id, String order_id, String category, String cart_type, String item_id, String quantity, String unit_price_incl_tax, String unit_price_exld_tax, String tax_rate, String tax_amt, String discount, String total_price, String color, String delivery_time, String order_status, String reason_for_cancel_or_return, String photo_cake_image, String delivery_charges, String date_of_order, String image_path) {
        this.checked = checked;
        this.product_name = product_name;
        this.id = id;
        this.user_id = user_id;
        this.order_id = order_id;
        this.category = category;
        this.cart_type = cart_type;
        this.item_id = item_id;
        this.quantity = quantity;
        this.unit_price_incl_tax = unit_price_incl_tax;
        this.unit_price_exld_tax = unit_price_exld_tax;
        this.tax_rate = tax_rate;
        this.tax_amt = tax_amt;
        this.discount = discount;
        this.total_price = total_price;
        this.color = color;
        this.delivery_time = delivery_time;
        this.order_status = order_status;
        this.reason_for_cancel_or_return = reason_for_cancel_or_return;
        this.photo_cake_image = photo_cake_image;
        this.delivery_charges = delivery_charges;
        this.date_of_order = date_of_order;
        this.image_path = image_path;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCart_type() {
        return cart_type;
    }

    public void setCart_type(String cart_type) {
        this.cart_type = cart_type;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit_price_incl_tax() {
        return unit_price_incl_tax;
    }

    public void setUnit_price_incl_tax(String unit_price_incl_tax) {
        this.unit_price_incl_tax = unit_price_incl_tax;
    }

    public String getUnit_price_exld_tax() {
        return unit_price_exld_tax;
    }

    public void setUnit_price_exld_tax(String unit_price_exld_tax) {
        this.unit_price_exld_tax = unit_price_exld_tax;
    }

    public String getTax_rate() {
        return tax_rate;
    }

    public void setTax_rate(String tax_rate) {
        this.tax_rate = tax_rate;
    }

    public String getTax_amt() {
        return tax_amt;
    }

    public void setTax_amt(String tax_amt) {
        this.tax_amt = tax_amt;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getReason_for_cancel_or_return() {
        return reason_for_cancel_or_return;
    }

    public void setReason_for_cancel_or_return(String reason_for_cancel_or_return) {
        this.reason_for_cancel_or_return = reason_for_cancel_or_return;
    }

    public String getPhoto_cake_image() {
        return photo_cake_image;
    }

    public void setPhoto_cake_image(String photo_cake_image) {
        this.photo_cake_image = photo_cake_image;
    }

    public String getDelivery_charges() {
        return delivery_charges;
    }

    public void setDelivery_charges(String delivery_charges) {
        this.delivery_charges = delivery_charges;
    }

    public String getDate_of_order() {
        return date_of_order;
    }

    public void setDate_of_order(String date_of_order) {
        this.date_of_order = date_of_order;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
