package com.innasoft.kilomart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Reciever.ConnectivityReceiver;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {



    @BindView(R.id.editoldpwd)
    TextInputEditText editoldpwd;
    @BindView(R.id.inputLayoutoldPass)
    TextInputLayout inputLayoutoldPass;
    @BindView(R.id.editnewPwd)
    TextInputEditText editnewPwd;
    @BindView(R.id.inputLayoutnewPass)
    TextInputLayout inputLayoutnewPass;
    @BindView(R.id.editSave)
    Button editSave;
    private PrefManager pref;
    String userId, mobile, username, imagePic, email,tokenValue,deviceId,gender;
    AppController appController;

    PrefManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Account");


        session = new PrefManager(this);
        appController = (AppController) getApplication();

        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        username = profile.get("name");
        mobile = profile.get("mobile");
        email = profile.get("email");
        imagePic = profile.get("profilepic");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");
        gender = profile.get("gender");


    }


    @OnClick({ R.id.editSave})
    public void onViewClicked(View view) {
        switch (view.getId()) {


            case R.id.editSave:
                // Method to manually check connection status
                boolean isConnected = appController.isConnection();

                if (isConnected) {
                    uploadFile(view);
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;

                    snackBar(message, color);
                    //showSnack(isConnected);
                }
                break;
        }
    }




    private void uploadFile(final View parentView) {


        String oldPassword = editoldpwd.getText().toString().trim();
        String newPassword = editnewPwd.getText().toString().trim();

        if (oldPassword.isEmpty()){
            Toast.makeText(getApplicationContext(),"Please Enter Old Password",Toast.LENGTH_SHORT).show();
            return;
        }
        if (newPassword.isEmpty()){
            Toast.makeText(getApplicationContext(),"Please Enter New Password",Toast.LENGTH_SHORT).show();
            return;
        }


       /* if (!validatePassword()) {
            return;
        }*/

       /* if (!NewPassword.equalsIgnoreCase(ConfirmPassword)) {
            Snackbar.make(parentView, "Password Doesn't Match !", Snackbar.LENGTH_SHORT).show();
            return;
        }*/


        final ProgressDialog progressDoalog = new ProgressDialog(ChangePasswordActivity.this);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();


        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userChangePassword(tokenValue,userId,oldPassword,newPassword);

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                progressDoalog.dismiss();
                BaseResponse baseResponse = response.body();


                if (response.isSuccessful()) {

                    if (baseResponse.getStatus().equalsIgnoreCase("10100")) {

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                Intent meService = new Intent(ChangePasswordActivity.this, MyAccountActivity.class);
                                meService.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(meService);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                            }
                        }, 2000);

                        // update session
                        session.createLogin(userId, username, email, mobile,null,deviceId,tokenValue,gender);

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    } else if (baseResponse.getStatus().equals("10200")){

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (baseResponse.getStatus().equals("10300")){

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    else if (baseResponse.getStatus().equals("10400")){

                        Toast.makeText(getApplicationContext(), baseResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressDoalog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }

    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}