package com.innasoft.kilomart;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.innasoft.kilomart.Adapters.LocationAdapter;
import com.innasoft.kilomart.Adapters.PaymentTpyeRecyclerAdapter;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Interface.PaymentTypeInterface;
import com.innasoft.kilomart.Response.CheckoutPostResponse;
import com.innasoft.kilomart.Response.CheckoutResponse;
import com.innasoft.kilomart.Response.LocationsResponse;
import com.innasoft.kilomart.Response.PayuMoneyPutResponse;
import com.innasoft.kilomart.Response.PayumoneyResponse;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;
import com.innasoft.kilomart.Storage.Utilities;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutActivity extends AppCompatActivity implements PaymentTypeInterface {
    PaymentTypeInterface paymentTypeInterface;

    public static final String TAG = "CheckoutActivity : ";
    @BindView(R.id.txtChangeAddress)
    TextView txtChangeAddress;
    @BindView(R.id.txtUserName)
    TextView txtUserName;
    @BindView(R.id.txtAddress)
    TextView txtAddress;
    @BindView(R.id.textView9)
    TextView textView9;
    @BindView(R.id.txtItems)
    TextView txtItems;
    @BindView(R.id.textView12)
    TextView textView12;
    @BindView(R.id.txtSubTotal)
    TextView txtSubTotal;
    @BindView(R.id.textView15)
    TextView textView15;
    @BindView(R.id.txtDiscount)
    TextView txtDiscount;
    @BindView(R.id.textView17)
    TextView textView17;
    @BindView(R.id.txtGrandTotal)
    TextView txtGrandTotal;
    @BindView(R.id.txt145)
    TextView txt145;
    @BindView(R.id.txtDeliver)
    TextView txtDeliver;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.textView21)
    TextView textView21;
    @BindView(R.id.txtAmountTobepaid)
    TextView txtAmountTobepaid;
    @BindView(R.id.txtPaymentMode)
    TextView txtPaymentMode;
    @BindView(R.id.bottamlayout)
    RelativeLayout bottamlayout;

    @BindView(R.id.btnOrder)
    Button btnOrder;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;
    @BindView(R.id.recycler_paymentoptions)
    RecyclerView recyclerPaymentoptions;

    private double price;
    AppController appController;
    @BindView(R.id.txtShippingAddress)
    TextView txtShippingAddress;
    @BindView(R.id.txtOrderDetails)
    TextView txtOrderDetails;
    @BindView(R.id.orderRelative)
    RelativeLayout orderRelative;

    PaymentTpyeRecyclerAdapter paymentTpyeRecyclerAdapter;
    /*@BindView(R.id.rbCod)
    RadioButton rbCod;
    @BindView(R.id.rbPayu)
    RadioButton rbPayu;*/

    private ProgressDialog pDialog;
    private PrefManager pref;
    String userId, addressid, payment_id, email, tokenValue, deviceId, loc_area, loc_pincode, shippingCharges;
    boolean cartStatus;

    String txnId;
    private AppCompatActivity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Checkout");

        appController = (AppController) getApplication();
        paymentTypeInterface = (PaymentTypeInterface) this;
        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        email = profile.get("email");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        // location Prefences
        SharedPreferences locationPref = getSharedPreferences(Utilities.PREFS_LOCATION, 0);
        boolean locationPrefBoolean = locationPref.getBoolean(Utilities.KEY_LOCATIONFIRSTTIME, false);
        loc_area = locationPref.getString(Utilities.KEY_AREA, "");
        loc_pincode = locationPref.getString(Utilities.KEY_PINCODE, "");
        shippingCharges = locationPref.getString(Utilities.KEY_SHIPPINGCHARGES, "");


        // transaction id
        txnId = System.currentTimeMillis() + "";

        if (getIntent().getExtras() != null) {
            addressid = getIntent().getStringExtra("addressId");
            cartStatus = getIntent().getBooleanExtra("Checkout", false);

        }


        if (appController.isConnection()) {

            // Showing progress dialog before making http request
            pDialog = new ProgressDialog(this);

            pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            pDialog.setIndeterminate(true);
            pDialog.setCancelable(false);
            pDialog.show();
            pDialog.setContentView(R.layout.my_progress);

           // showLocations();
            checkoutData();
            init();


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            // app.internetDilogue(KitchenitemListActivity.this);

        }

    }

    private void init() {
        activity = CheckoutActivity.this;

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PaymentData();

            }
        });
    }

    // checkout response
    private void checkoutData() {

        Call<CheckoutResponse> call = RetrofitClient.getInstance().getApi().checkOutStatus(tokenValue, userId, addressid);
        call.enqueue(new Callback<CheckoutResponse>() {
            @Override
            public void onResponse(Call<CheckoutResponse> call, Response<CheckoutResponse> response) {

                hidePDialog();

                CheckoutResponse cartResponse = response.body();

                List<CheckoutResponse.DataBean.AddressBean> addressBeans = cartResponse.getData().getAddress();
                List<CheckoutResponse.DataBean.PaymentGatewayBean> paymentGatewayBeans = cartResponse.getData().getPayment_gateway();

                if (response.isSuccessful()) {
                    if (cartResponse.getStatus().equalsIgnoreCase("10100")) {


                        txtSubTotal.setText("\u20B9" + cartResponse.getData().getFinal_gross_amount());
                        txtDiscount.setText("\u20B9" + cartResponse.getData().getFinal_discount());
                        txtGrandTotal.setText("\u20B9" + cartResponse.getData().getPayable_amount());
                        /*txtDeliver.setText("\u20B9" + shippingCharges);

                        double paybleAmount = Double.parseDouble(cartResponse.getData().getPayable_amount()) + Double.parseDouble(shippingCharges);

                        txtAmountTobepaid.setText("\u20B9" + paybleAmount);*/


                        if (cartResponse.getData().getAddress().size() != 0) {


                            addressid = cartResponse.getData().getAddress().get(0).getId();

                            // username and address
                            txtUserName.setText(cartResponse.getData().getAddress().get(0).getName());
                            txtAddress.setText(cartResponse.getData().getAddress().get(0).getAddress_line1() + "," + cartResponse.getData().getAddress().get(0).getAddress_line2() + "," + cartResponse.getData().getAddress().get(0).getArea() + "," + cartResponse.getData().getAddress().get(0).getCity() + "," +
                                    cartResponse.getData().getAddress().get(0).getState() + "," + cartResponse.getData().getAddress().get(0).getPincode());


                            showLocations(addressBeans.get(0).getPincode(), cartResponse.getData().getPayable_amount());


                        } else {
                            txtAddress.setText("Please Add Address !! ");
                            txtAddress.setTextColor(Color.RED);
                            txtAddress.setTextSize(20);


                        }

                        // payment gateway array

                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerPaymentoptions.setLayoutManager(layoutManager);
                        recyclerPaymentoptions.setItemAnimator(new DefaultItemAnimator());

                        paymentTpyeRecyclerAdapter = new PaymentTpyeRecyclerAdapter(CheckoutActivity.this, paymentGatewayBeans, paymentTypeInterface);
                        recyclerPaymentoptions.setAdapter(paymentTpyeRecyclerAdapter);


                        // Change Address
                        txtChangeAddress.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(CheckoutActivity.this, AddressListActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("Checkout", cartStatus);
                                activity.startActivity(intent);
                                activity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        });




                    } else if (cartResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), cartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (cartResponse.getStatus().equals("10300")) {

                        Toast.makeText(getApplicationContext(), cartResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (cartResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), cartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<CheckoutResponse> call, Throwable t) {
                hidePDialog();
            }
        });


    }

    @Override
    public void onItemClick(List<CheckoutResponse.DataBean.PaymentGatewayBean> paymentGatewayBean, int position) {
        payment_id = paymentGatewayBean.get(position).getId();
        // Toast.makeText(getApplicationContext(),""+paymentGatewayBean.get(position).getId(),Toast.LENGTH_SHORT).show();
    }

    private void showLocations(final String pincode, final String payable_amount) {

        Call<LocationsResponse> call = RetrofitClient.getInstance().getApi().getLocations();
        call.enqueue(new Callback<LocationsResponse>() {
            @Override
            public void onResponse(Call<LocationsResponse> call, retrofit2.Response<LocationsResponse> response) {

                LocationsResponse addressResponse = response.body();

                if (response.isSuccessful()) {


                    if (addressResponse.getStatus().equalsIgnoreCase("10100")) {


                        List<LocationsResponse.DataBean> locationItemsArray = response.body() != null ? response.body().getData() : null;

                        for (LocationsResponse.DataBean dataBean : locationItemsArray) {

                            if (pincode.equalsIgnoreCase(dataBean.getPincode())) {

                                Log.d(TAG, "onResponse: " + pincode + "-- " + dataBean.getPincode());
                                Log.d(TAG, "onResponse2: " + pincode + "-- " + dataBean.getShipping_charges());


                                try {
                                    double payAmount = DecimalFormat.getNumberInstance().parse(payable_amount).doubleValue();

                                    if (payAmount>1500){

                                        double paybleAmount = payAmount + 0.00;
                                       // txtDeliver.setText("\u20B9" + "0.00");
                                        txtDeliver.setText("Free Delivery");
                                        txtAmountTobepaid.setText("\u20B9" + paybleAmount+"0");
                                        System.out.println(paybleAmount); //111111.23
                                    }else {
                                        double paybleAmount = payAmount + ParseDouble(dataBean.getShipping_charges());
                                        txtDeliver.setText("\u20B9" + dataBean.getShipping_charges());
                                        txtAmountTobepaid.setText("\u20B9" + paybleAmount+"0");
                                        System.out.println(paybleAmount); //111111.23
                                    }


                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }
                        }


                    }


                }

            }

            @Override
            public void onFailure(Call<LocationsResponse> call, Throwable t) {

            }
        });

    }

    private void PaymentData() {

        if (addressid == null) {
            Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + "Please Choose Address" + "</font>"), Snackbar.LENGTH_SHORT).show();

            //Toast.makeText(CheckoutActivity.this, "Please Choose Address ", Toast.LENGTH_SHORT).show();
            return;
        }

        if (payment_id == null) {
            Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + "Please Select Payment method" + "</font>"), Snackbar.LENGTH_SHORT).show();

            //Toast.makeText(CheckoutActivity.this, "Please Select Payment method ", Toast.LENGTH_SHORT).show();
            return;
        }


        postOrder(addressid, payment_id);

    }


    // order post
    private void postOrder(final String addressid, String selectedId) {
// Showing progress dialog before making http request
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading..");
        pDialog.show();

        Call<CheckoutPostResponse> call = RetrofitClient.getInstance().getApi().checkoutPost(tokenValue, userId, addressid, "", String.valueOf(selectedId), "ANDROID");
        call.enqueue(new Callback<CheckoutPostResponse>() {
            @Override
            public void onResponse(Call<CheckoutPostResponse> call, Response<CheckoutPostResponse> response) {

                hidePDialog();

                final CheckoutPostResponse invoicePostResponse = response.body();

                if (response.isSuccessful()) {

                    if (invoicePostResponse.getStatus().equalsIgnoreCase("10100")) {

                        showCustomDialog(invoicePostResponse.getOrder_id(), invoicePostResponse.getMessage());

                        // cart update
                        appController.cartCount(userId, deviceId);


                    } else if (invoicePostResponse.getStatus().equals("10200")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                       // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (invoicePostResponse.getStatus().equals("10300")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                        //Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (invoicePostResponse.getStatus().equals("10400")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.getMessage()+ "</font>"), Snackbar.LENGTH_SHORT).show();

                       // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (invoicePostResponse.getStatus().equals("10500")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                       // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (invoicePostResponse.getStatus().equals("10600")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                       // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (invoicePostResponse.getStatus().equals("10700")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                       // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (invoicePostResponse.getStatus().equals("10800")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                       // Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();


                    } else if (invoicePostResponse.getStatus().equals("10900")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + invoicePostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                        //Toast.makeText(getApplicationContext(), invoicePostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<CheckoutPostResponse> call, Throwable t) {
                hidePDialog();
            }
        });
    }

    private void showCustomDialog(final int order_id, String message) {
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false);


        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);

        TextView txtmessage = (TextView) dialogView.findViewById(R.id.txtMessage);
        txtmessage.setText(message);


        Button buttonOk = (Button) dialogView.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent order_detail = new Intent(CheckoutActivity.this, OrdersListActivity.class);
                order_detail.putExtra("Order_ID", String.valueOf(order_id));
                order_detail.putExtra("Checkout", cartStatus);
                order_detail.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(order_detail);
                finish();
            }
        });

        //finally creating the alert dialog and displaying it
        AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        hidePDialog();
    }

    private void hidePDialog() {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    private double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
