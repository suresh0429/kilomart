package com.innasoft.kilomart;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.innasoft.kilomart.Adapters.ViewPagerAdapter;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Helper.Converter;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Response.ProductSingleResponse;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;
import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.kilomart.Apis.RetrofitClient.PRODUCT_IMAGE_BASE_URL2;
import static com.innasoft.kilomart.Storage.Utilities.capitalize;

public class ProductDetailsActivity extends AppCompatActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;
    @BindView(R.id.txtProductName)
    TextView txtProductName;
    @BindView(R.id.txtPrice)
    TextView txtPrice;
    @BindView(R.id.txtDisPrice)
    TextView txtDisPrice;
    @BindView(R.id.txtDescription)
    TextView txtDescription;
    @BindView(R.id.btnGotoCart)
    Button btnGotoCart;
    @BindView(R.id.btnAddtocart)
    Button btnAddtocart;
    @BindView(R.id.bootamLayout)
    CardView bootamLayout;
    @BindView(R.id.progressBarMain)
    ProgressBar progressBarMain;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;

    AppController appController;
    List<String> viewPagerItemslist = new ArrayList<>();
    @BindView(R.id.product_minus)
    TextView productMinus;
    @BindView(R.id.product_quantity)
    TextView productQuantity;
    @BindView(R.id.product_plus)
    TextView productPlus;
    @BindView(R.id.txtWeight)
    TextView txtWeight;

    private PrefManager pref;
    String userid;
    int cartindex;
    int count = 1;

    String id, title, deviceId, tokenValue;
    String productIdsSet;
    String cartTypeSet;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        loadOncreateData();


    }

    private void loadOncreateData() {
        if (getIntent().getExtras() != null) {
            id = getIntent().getStringExtra("productId");
            Log.e("PRODUCTID", "" + id);
            title = getIntent().getStringExtra("productName");
        }
        getSupportActionBar().setTitle(capitalize(title));
        appController = (AppController) getApplicationContext();

        pref = new PrefManager(getApplicationContext());

        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userid = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (appController.isConnection()) {

            progressBarMain.setVisibility(View.VISIBLE);

            prepareProductDetailsData(id, deviceId);

            // cart count
            appController.cartCount(userid, deviceId);
            SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
            cartindex = preferences.getInt("itemCount", 0);
            productIdsSet = preferences.getString("productIds", null);
            cartTypeSet = preferences.getString("cartType", null);


            Log.e("productIdsSet", "" + productIdsSet + "------" + cartTypeSet);


            invalidateOptionsMenu();


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            // app.internetDilogue(KitchenitemListActivity.this);

        }


    }


    private void prepareProductDetailsData(final String id, String deviceId) {

        progressBarMain.setVisibility(View.VISIBLE);
        Call<ProductSingleResponse> call = RetrofitClient.getInstance().getApi().getProductdetails(id, deviceId);

        call.enqueue(new Callback<ProductSingleResponse>() {
            @Override
            public void onResponse(Call<ProductSingleResponse> call, Response<ProductSingleResponse> response) {


                ProductSingleResponse productResponse = response.body();

                if (response.isSuccessful()) {
                    progressBarMain.setVisibility(View.GONE);

                    if (productResponse.getStatus().equalsIgnoreCase("10100")) {

                        final ProductSingleResponse.DataBean product = response.body().getData();


                        txtProductName.setText(capitalize(product.getProduct_name()));

                        txtPrice.setText(getResources().getString(R.string.Rs) + product.getSelling_price());
                        txtWeight.setText( product.getUnit_value() + product.getUnit_name());

                        txtDisPrice.setText(getResources().getString(R.string.Rs) + product.getMrp_price() );
                        txtDisPrice.setPaintFlags(txtDisPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        txtDisPrice.setTextColor(Color.RED);
                        txtDescription.setText(product.getSeo_description());

                        // check avalibulity
                        if (product.getAvailability().equalsIgnoreCase("1")){
                            btnAddtocart.setClickable(false);
                            btnAddtocart.setEnabled(false);
                            btnAddtocart.setBackgroundColor(Color.parseColor("#C0C0C0"));
                        }


                        // slider Images
                        List<String> namesList = product.getImages();

                        Log.e("ALLIMAGESLIDES", "" + namesList.size());


                        viewPagerItemslist.clear();
                        for (String name : namesList) {

                            System.out.println(name);

                            Log.e("IMAGESLIDES", "" + name);

                            viewPagerItemslist.add(PRODUCT_IMAGE_BASE_URL2 + name);
                        }

                        final PageIndicatorView pageIndicatorView = findViewById(R.id.pageIndicatorView);
                        ViewPagerAdapter adapter = new ViewPagerAdapter(getApplicationContext(), viewPagerItemslist,product.getAvailability());
                        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
                        viewPager.setAdapter(adapter);
                        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position) {
                                pageIndicatorView.setSelection(position);
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });


                        // quantity decrease
                        productMinus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (count > 1) {
                                    count--;
                                    productQuantity.setText("" + count);
                                }
                            }
                        });

                        // quantity increase
                        productPlus.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                count++;
                                productQuantity.setText("" + count);
                            }
                        });


                        btnAddtocart.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                addtocartData(product.getId(), productQuantity.getText().toString());

                            }
                        });

                        btnGotoCart.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        });


                    } else if (productResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), productResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (productResponse.getStatus().equals("10300")) {

                        Toast.makeText(getApplicationContext(), productResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (productResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), productResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }
            }

            @Override
            public void onFailure(Call<ProductSingleResponse> call, Throwable t) {
                progressBarMain.setVisibility(View.GONE);

            }
        });


    }


    // submit details
    private void addtocartData(final String productid, String quantity) {

        progressBarMain.setVisibility(View.VISIBLE);

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().addtoCart(productid, deviceId, userid, quantity);

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                progressBarMain.setVisibility(View.GONE);

                BaseResponse cartPostResponse = response.body();

                if (response.isSuccessful()) {

                    if (cartPostResponse.getStatus().equalsIgnoreCase("10100")) {

                        // Toast.makeText(ProductDetailsActivity.this, cartPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.WHITE + "\">" + cartPostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                        // cart count updation
                        cartindex = cartindex + 1;
                        Log.e("CARTPLUSCOUNT", "" + cartindex);
                        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putInt("itemCount", cartindex);
                        editor.apply();


                        // cart count Update
                        appController.cartCount(userid, deviceId);
                        invalidateOptionsMenu();

                    } else if (cartPostResponse.getStatus().equals("10200")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + cartPostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                        //  Toast.makeText(getApplicationContext(), cartPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (cartPostResponse.getStatus().equals("10300")) {
                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + cartPostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();

                        //  Toast.makeText(getApplicationContext(), cartPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (cartPostResponse.getStatus().equals("10400")) {

                        Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + cartPostResponse.getMessage() + "</font>"), Snackbar.LENGTH_SHORT).show();


                        // Toast.makeText(getApplicationContext(), cartPostResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressBarMain.setVisibility(View.GONE);
            }
        });
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        loadOncreateData();
        appController.cartCount(userid, deviceId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
    }

    @Override
    protected void onStart() {
        super.onStart();

        //  loadOncreateData();
        appController.cartCount(userid, deviceId);
        SharedPreferences preferences = getSharedPreferences("CARTCOUNT", 0);
        cartindex = preferences.getInt("itemCount", 0);
        Log.e("cartindexonstart", "" + cartindex);
        invalidateOptionsMenu();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        final MenuItem menuItem = menu.findItem(R.id.action_cart);
        menuItem.setIcon(Converter.convertLayoutToImage(ProductDetailsActivity.this, cartindex, R.drawable.ic_actionbar_bag));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.action_cart:
                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            case R.id.action_search:
                Intent intent1 = new Intent(ProductDetailsActivity.this, SearchActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
