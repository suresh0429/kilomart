package com.innasoft.kilomart.Response;

import java.util.List;

public class BannersResponse {


    /**
     * status : 10100
     * message : Data fetch successfully.
     * data : [{"id":"0","highlighted_text":"Promo banner","image":"images/banners/app/c0b57-5357a-4.png"}]
     */

    private String status;
    private String message;
    private List<DataBean> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 0
         * highlighted_text : Promo banner
         * image : images/banners/app/c0b57-5357a-4.png
         */

        private String id;
        private String highlighted_text;
        private String image;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getHighlighted_text() {
            return highlighted_text;
        }

        public void setHighlighted_text(String highlighted_text) {
            this.highlighted_text = highlighted_text;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }
    }
}
