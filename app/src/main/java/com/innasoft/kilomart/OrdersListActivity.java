package com.innasoft.kilomart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.innasoft.kilomart.Adapters.OrdersListAdapter;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Response.OrderListResponse;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrdersListActivity extends AppCompatActivity {
    AppController appController;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txtSubtotal)
    TextView txtSubtotal;
    @BindView(R.id.txtDeliverCharges)
    TextView txtDeliverCharges;
    @BindView(R.id.txtamntPaid)
    TextView txtamntPaid;
    @BindView(R.id.txtPayMode)
    TextView txtPayMode;
    @BindView(R.id.layoutParent)
    LinearLayout layoutParent;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.order_status)
    TextView orderStatus;
    @BindView(R.id.txtExpectedDate)
    TextView txtExpectedDate;

    private PrefManager pref;
    OrdersListAdapter ordersListAdapter;

    String userId, tokenValue, deviceId, orderId;
    String[] descriptionData = {"OrderPlace", "InProgress", "Shipped", "delivered", "Completed"};
    boolean cartStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_list);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Orders Items");
        appController = (AppController) getApplication();


        // Displaying user information from shared preferences
        pref = new PrefManager(getApplicationContext());

        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");

        if (getIntent() != null) {
            orderId = getIntent().getStringExtra("Order_ID");
            cartStatus = getIntent().getBooleanExtra("Checkout", false);
        }

        if (appController.isConnection()) {

            prepareOrderListData();

        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }
    }


    private void prepareOrderListData() {

        progressBar.setVisibility(View.VISIBLE);

        Call<OrderListResponse> call = RetrofitClient.getInstance().getApi().getOrderList(tokenValue, userId, orderId);
        call.enqueue(new Callback<OrderListResponse>() {
            @Override
            public void onResponse(Call<OrderListResponse> call, Response<OrderListResponse> response) {
                if (response.isSuccessful()) ;
                OrderListResponse orderListResponse = response.body();
                if (orderListResponse.getStatus().equals("10100")) {
                    progressBar.setVisibility(View.GONE);
//                    Toast.makeText(OrdersListActivity.this, orderListResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    OrderListResponse.DataBean.OrderDetailsBean orderDetailsBean = orderListResponse.getData().getOrderDetails();
                    txtSubtotal.setText(getResources().getString(R.string.Rs) + " " + orderDetailsBean.getTotalMrpPrice());
                    txtDeliverCharges.setText(getResources().getString(R.string.Rs) + " " + orderDetailsBean.getShippingCharges());
                    txtPayMode.setText("Payment Mode : " + orderDetailsBean.getGetawayName());
                    orderStatus.setText("Order status : " + orderDetailsBean.getOrderStatus());
                    txtExpectedDate.setText("Expected Delivery Date : "+orderDetailsBean.getExpectedDelivery());
                    txtamntPaid.setText(getResources().getString(R.string.Rs) + " " + orderDetailsBean.getFinalPrice());

                    List<OrderListResponse.DataBean.ProductsBean> productsBeanList = orderListResponse.getData().getProducts();

                    RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    ordersListAdapter = new OrdersListAdapter(OrdersListActivity.this, productsBeanList);
                    recyclerView.setAdapter(ordersListAdapter);
                } else if (orderListResponse.getStatus().equals("10200")) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(OrdersListActivity.this, orderListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (orderListResponse.getStatus().equals("10300")) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(OrdersListActivity.this, orderListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                } else if (orderListResponse.getStatus().equals("10400")) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(OrdersListActivity.this, orderListResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<OrderListResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(OrdersListActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                if (cartStatus) {
                    Intent intent = new Intent(OrdersListActivity.this, HomeActivity.class);
                    intent.putExtra("Checkout", cartStatus);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(OrdersListActivity.this, OrderIdActvity.class);
                    intent.putExtra("Checkout", cartStatus);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();

        if (cartStatus) {
            Intent intent = new Intent(OrdersListActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Checkout", cartStatus);
            startActivity(intent);
        } else {
            Intent intent = new Intent(OrdersListActivity.this, OrderIdActvity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("Checkout", cartStatus);
            startActivity(intent);
        }
    }


}
