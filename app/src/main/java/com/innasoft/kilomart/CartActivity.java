package com.innasoft.kilomart;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.innasoft.kilomart.Adapters.CartAdapter;
import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Interface.CartProductClickListener;
import com.innasoft.kilomart.Model.Product;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Response.CartResponse;
import com.innasoft.kilomart.Response.WishListPostResponse;
import com.innasoft.kilomart.Singleton.AppController;
import com.innasoft.kilomart.Storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity implements CartProductClickListener {

    @BindView(R.id.productsRcycler)
    RecyclerView productsRcycler;
    @BindView(R.id.txtSubTotal)
    TextView txtSubTotal;
    @BindView(R.id.btnCheckOut)
    Button btnCheckOut;
    @BindView(R.id.bottamlayout)
    LinearLayout bottamlayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.parentLayout)
    RelativeLayout parentLayout;


    ArrayList<Product> productArrayList;
    CartProductClickListener cartProductClickListener;
    CartAdapter cartAdapter;
    AppController appController;
    @BindView(R.id.txtAlert)
    TextView txtAlert;
    private PrefManager pref;

    String userId, tokenValue, deviceId;
    //int totalQty = 0;
    int totalQuantity = 0;
    int minimumOrder = 500;

    public String length;
    public String module;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Cart");

        cartProductClickListener = (CartProductClickListener) this;

        appController = (AppController) getApplication();

        pref = new PrefManager(getApplicationContext());
        // Displaying user information from shared preferences
        HashMap<String, String> profile = pref.getUserDetails();
        userId = profile.get("id");
        tokenValue = profile.get("AccessToken");
        deviceId = profile.get("deviceId");


        if (appController.isConnection()) {

            // Showing progress dialog before making http request

            progressBar.setVisibility(View.VISIBLE);

            // cart count Update
            appController.cartCount(userId, deviceId);

            cartData();


        } else {

            setContentView(R.layout.internet);

            Button tryButton = (Button) findViewById(R.id.btnTryagain);
            tryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    invalidateOptionsMenu();
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });

            // app.internetDilogue(KitchenitemListActivity.this);

        }


    }


    @Override
    public void onMinusClick(Product cartItemsBean) {
        int i = productArrayList.indexOf(cartItemsBean);

        if (cartItemsBean.getPurchase_quantity() > 1) {


            Product updatedProduct = new Product(cartItemsBean.getId(), cartItemsBean.getProduct_id(), cartItemsBean.getProduct_name(),
                    cartItemsBean.getSelling_price(), cartItemsBean.getMrp_price(), (cartItemsBean.getPurchase_quantity() - 1), cartItemsBean.getUnit_id(),
                    cartItemsBean.getUnit_name(), cartItemsBean.getUnit_value(), cartItemsBean.getBrand_id(), cartItemsBean.getBrand_name(), cartItemsBean.getNet_amount(),
                    cartItemsBean.getGross_amount(), cartItemsBean.getImages());

            productArrayList.remove(cartItemsBean);
            productArrayList.add(i, updatedProduct);

            updateQuantity(updatedProduct);
            cartAdapter.notifyDataSetChanged();


            calculateCartTotal();

        }
    }

    @Override
    public void onPlusClick(Product cartItemsBean) {
        int i = productArrayList.indexOf(cartItemsBean);

        Product updatedProduct = new Product(cartItemsBean.getId(), cartItemsBean.getProduct_id(), cartItemsBean.getProduct_name(),
                cartItemsBean.getSelling_price(), cartItemsBean.getMrp_price(), (cartItemsBean.getPurchase_quantity() + 1), cartItemsBean.getUnit_id(),
                cartItemsBean.getUnit_name(), cartItemsBean.getUnit_value(), cartItemsBean.getBrand_id(), cartItemsBean.getBrand_name(), cartItemsBean.getNet_amount(),
                cartItemsBean.getGross_amount(), cartItemsBean.getImages());

        productArrayList.remove(cartItemsBean);
        productArrayList.add(i, updatedProduct);

        Log.e("QUNATITY", "" + updatedProduct.getPurchase_quantity());
        updateQuantity(updatedProduct);

        cartAdapter.notifyDataSetChanged();


        calculateCartTotal();
    }

    @Override
    public void onSaveClick(Product product) {

    }

    @Override
    public void onRemoveDialog(final Product product) {
        final Dialog dialog = new Dialog(CartActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_dialog);


        // set the custom dialog components - text, image and button
        TextView te = (TextView) dialog.findViewById(R.id.txtAlert);
        te.setText("Are you want to Delete?");

        TextView yes = (TextView) dialog.findViewById(R.id.btnYes);
        TextView no = (TextView) dialog.findViewById(R.id.btnNo);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete(product);
                cartAdapter.notifyDataSetChanged();
                dialog.dismiss();
                Snackbar.make(parentLayout, "Deleted Successfully", Snackbar.LENGTH_SHORT).show();


            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onWishListDialog(final Product product) {

        final Dialog dialog = new Dialog(CartActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_dialog);


        // set the custom dialog components - text, image and button
        TextView te = (TextView) dialog.findViewById(R.id.txtAlert);
        te.setText("Add to WishList?");

        TextView yes = (TextView) dialog.findViewById(R.id.btnYes);
        TextView no = (TextView) dialog.findViewById(R.id.btnNo);

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveWishList(product);
                cartAdapter.notifyDataSetChanged();
                dialog.dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    // Remove from Cart
    public void delete(final Product product) {
        // Showing progress dialog before making http request
        progressBar.setVisibility(View.VISIBLE);

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().deleteCart(userId, product.getId(), deviceId);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                progressBar.setVisibility(View.GONE);
                BaseResponse wishListPostResponse = response.body();

                if (response.isSuccessful()) {

                    if (wishListPostResponse.getStatus().equalsIgnoreCase("10100")) {

                        int i = productArrayList.indexOf(product);
                        if (i == -1) {
                            throw new IndexOutOfBoundsException();
                        }
                        productArrayList.remove(productArrayList.get(i));

                        // cart count Update
                        appController.cartCount(userId, deviceId);
                        invalidateOptionsMenu();

                        calculateCartTotal();
                        cartAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

                progressBar.setVisibility(View.GONE);
            }
        });
    }

    // Save Wishlist
    public void saveWishList(final Product product) {

        progressBar.setVisibility(View.VISIBLE);

        Call<WishListPostResponse> call = RetrofitClient.getInstance().getApi().addWishList(product.getBrand_id(), userId, product.getBrand_id(), "move");
        call.enqueue(new Callback<WishListPostResponse>() {
            @Override
            public void onResponse(Call<WishListPostResponse> call, Response<WishListPostResponse> response) {

                progressBar.setVisibility(View.GONE);

                WishListPostResponse wishListPostResponse = response.body();

                if (response.isSuccessful()) {

                    if ((wishListPostResponse != null ? wishListPostResponse.getStatus() : 0) == 1) {

                        int i = productArrayList.indexOf(product);
                        if (i == -1) {
                            throw new IndexOutOfBoundsException();
                        }
                        productArrayList.remove(productArrayList.get(i));

                        Snackbar.make(parentLayout, "Item Added to Wishlist", Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(parentLayout, "Item already Added to Wishlist", Snackbar.LENGTH_SHORT).show();


                    }

                    // cart count Update
                    appController.cartCount(userId, deviceId);
                    invalidateOptionsMenu();

                    calculateCartTotal();
                    cartAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<WishListPostResponse> call, Throwable t) {

                progressBar.setVisibility(View.GONE);
            }
        });
    }


    private void cartData() {

        productArrayList = new ArrayList<>();

        Call<CartResponse> call = RetrofitClient.getInstance().getApi().cartItemsList(userId, deviceId);
        call.enqueue(new Callback<CartResponse>() {
            @Override
            public void onResponse(Call<CartResponse> call, Response<CartResponse> response) {

                progressBar.setVisibility(View.GONE);

                final CartResponse cartResponse = response.body();

                if (response.isSuccessful()) {


                    assert cartResponse != null;
                    if (cartResponse.getStatus().equalsIgnoreCase("10100")) {

                        for (CartResponse.DataBean.RecordsBean cartItemsBean : cartResponse.getData().getRecords()) {


                            productArrayList.add(new Product(cartItemsBean.getCart_id(), cartItemsBean.getProduct_id(), cartItemsBean.getProduct_name(),
                                    cartItemsBean.getSelling_price(), cartItemsBean.getMrp_price(), Integer.parseInt(cartItemsBean.getPurchase_quantity()), cartItemsBean.getUnit_id(),
                                    cartItemsBean.getUnit_name(), cartItemsBean.getUnit_value(), cartItemsBean.getBrand_id(), cartItemsBean.getBrand_name(), cartItemsBean.getNet_amount(),
                                    cartItemsBean.getGross_amount(), cartItemsBean.getImages()));


                            Log.e("LENGTH", "" + productArrayList.size());

                            length = String.valueOf(productArrayList.size());


                        }

                        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        productsRcycler.setLayoutManager(mLayoutManager1);
                        productsRcycler.setItemAnimator(new DefaultItemAnimator());
                        productsRcycler.setHasFixedSize(true);

                        cartAdapter = new CartAdapter(CartActivity.this, productArrayList, cartProductClickListener);
                        productsRcycler.setAdapter(cartAdapter);
                        cartAdapter.notifyDataSetChanged();
                        // calculate total amount
                        calculateCartTotal();


                    } else if (cartResponse.getStatus().equals("10200")) {

                        Toast.makeText(getApplicationContext(), cartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (cartResponse.getStatus().equals("10300")) {

                        setContentView(R.layout.emptycart);
                        Button btnGotohome = (Button) findViewById(R.id.btnGotohome);
                        btnGotohome.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Intent intent = new Intent(CartActivity.this, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        });

                    } else if (cartResponse.getStatus().equals("10400")) {

                        Toast.makeText(getApplicationContext(), cartResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
            }

            @Override
            public void onFailure(Call<CartResponse> call, Throwable t) {

                progressBar.setVisibility(View.GONE);
                Snackbar.make(parentLayout, Html.fromHtml("<font color=\"" + Color.RED + "\">" + getResources().getString(R.string.slowInternetconnection) + "</font>"), Snackbar.LENGTH_SHORT).show();

            }
        });


    }

    // update quantity
    public void updateQuantity(final Product product) {

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().updateQuantity(product.getId(), String.valueOf(product.getPurchase_quantity()));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (response.isSuccessful()) {

                    BaseResponse baseResponse = response.body();

                    if (baseResponse.getStatus().equalsIgnoreCase("10100")) {

                        //Toast.makeText(getApplicationContext(),baseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                    } else if (baseResponse.getStatus().equalsIgnoreCase("10200")) {
                        // Toast.makeText(getApplicationContext(),baseResponse.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }


            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });
    }


    // total Amount
    public void calculateCartTotal() {

        int grandTotal = 0;

        for (Product order : productArrayList) {

            grandTotal += (ParseDouble(order.getSelling_price()) * order.getPurchase_quantity());

        }

        Log.e("TOTAL", "" + productArrayList.size() + "Items | " + "  DISCOUNT : " + grandTotal);

        txtSubTotal.setText(productArrayList.size() + " Items | Rs " + grandTotal);

        if (grandTotal >= minimumOrder) {

            btnCheckOut.setVisibility(View.VISIBLE);
            txtAlert.setVisibility(View.GONE);
        } else {

            btnCheckOut.setVisibility(View.INVISIBLE);
            txtAlert.setVisibility(View.VISIBLE);
            txtAlert.setText("Minimum Order Must Be Greater Than Rs."+minimumOrder+"*");

        }

        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(CartActivity.this, CheckoutActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("Checkout",true);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });


        if (productArrayList.size() == 0) {

            setContentView(R.layout.emptycart);
            Button btnGotohome = (Button) findViewById(R.id.btnGotohome);
            btnGotohome.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(CartActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
            });
        }

    }


    private double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                return Double.parseDouble(strNumber);
            } catch (Exception e) {
                return -1;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        } else return 0;
    }


    @Override
    protected void onRestart() {
        super.onRestart();

        // cart count Update
        appController.cartCount(userId, deviceId);

        cartData();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // cart count Update
        appController.cartCount(userId, deviceId);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                invalidateOptionsMenu();
                onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


}
