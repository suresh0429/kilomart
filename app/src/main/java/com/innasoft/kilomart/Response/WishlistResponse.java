package com.innasoft.kilomart.Response;

import java.util.List;

public class WishlistResponse {


    /**
     * status : 1
     * wishList : [{"id":"2019","product_id":"PIK727DH6","item_id":"354","image":"https://pickany24x7.com/stationery/stationery/../images/product_images/PIK727DH6/1.jpg","product_name":"Mathematical Instrument Box","inclTax":"150.00","dis_price":"135","dis_percent":"10","category":"stationery","status":"0"}]
     * message : Wish List Items
     */

    private int status;
    private String message;
    private List<WishListBean> wishList;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WishListBean> getWishList() {
        return wishList;
    }

    public void setWishList(List<WishListBean> wishList) {
        this.wishList = wishList;
    }

    public static class WishListBean {
        /**
         * id : 2019
         * product_id : PIK727DH6
         * item_id : 354
         * image : https://pickany24x7.com/stationery/stationery/../images/product_images/PIK727DH6/1.jpg
         * product_name : Mathematical Instrument Box
         * inclTax : 150.00
         * dis_price : 135
         * dis_percent : 10
         * category : stationery
         * status : 0
         */

        private String id;
        private String product_id;
        private String item_id;
        private String image;
        private String product_name;
        private String inclTax;
        private String dis_price;
        private String dis_percent;
        private String category;
        private String status;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getItem_id() {
            return item_id;
        }

        public void setItem_id(String item_id) {
            this.item_id = item_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getProduct_name() {
            return product_name;
        }

        public void setProduct_name(String product_name) {
            this.product_name = product_name;
        }

        public String getInclTax() {
            return inclTax;
        }

        public void setInclTax(String inclTax) {
            this.inclTax = inclTax;
        }

        public String getDis_price() {
            return dis_price;
        }

        public void setDis_price(String dis_price) {
            this.dis_price = dis_price;
        }

        public String getDis_percent() {
            return dis_percent;
        }

        public void setDis_percent(String dis_percent) {
            this.dis_percent = dis_percent;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
