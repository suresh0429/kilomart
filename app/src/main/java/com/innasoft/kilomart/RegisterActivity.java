package com.innasoft.kilomart;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.innasoft.kilomart.Apis.RetrofitClient;
import com.innasoft.kilomart.Reciever.ConnectivityReceiver;
import com.innasoft.kilomart.Response.BaseResponse;
import com.innasoft.kilomart.Singleton.AppController;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {
    boolean isConnected;
    String agree;
    String OTP;

    @BindView(R.id.imageView)
    AppCompatImageView imageView;
    @BindView(R.id.etUsername)
    TextInputEditText etUsername;
    @BindView(R.id.user_til)
    TextInputLayout userTil;
    @BindView(R.id.etPhone)
    TextInputEditText etPhone;
    @BindView(R.id.mobile_til)
    TextInputLayout mobileTil;
    @BindView(R.id.etEmail)
    TextInputEditText etEmail;
    @BindView(R.id.email_til)
    TextInputLayout emailTil;
    @BindView(R.id.etPassword)
    TextInputEditText etPassword;
    @BindView(R.id.pwd_til)
    TextInputLayout pwdTil;
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.txtSignin)
    TextView txtSignin;
    @BindView(R.id.parentLayout)
    NestedScrollView parentLayout;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    AppController app;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

         app = (AppController) getApplication();

        String next = "Already Registered ? <font color='#f89113'>Login</font> me";
        txtSignin.setText(Html.fromHtml(next));

        String checktext = "I agree to the Kilomart ? <font color='#f89113'>Terms & conditions</font> and <font color='#f89113'>Privacy Policy</font>";
        checkbox.setText(Html.fromHtml(checktext));

        agree = "0";
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    agree = "1";
                } else {
                    agree = "0";
                }
            }
        });


        etUsername.addTextChangedListener(new MyTextWatcher(etUsername));
        etPhone.addTextChangedListener(new MyTextWatcher(etPhone));
        etEmail.addTextChangedListener(new MyTextWatcher(etEmail));
        etPassword.addTextChangedListener(new MyTextWatcher(etPassword));


    }




    // user Registration
    private void userRegistartion(){

        final String name = etUsername.getText().toString().trim();
        final String mobile = etPhone.getText().toString().trim();
        final String email = etEmail.getText().toString().trim();
        final String password = etPassword.getText().toString().trim();

        if ((!isValidName(name))) {
            return;
        }
        if ((!isValidPhoneNumber(mobile))) {
            return;
        }
        if ((!isValidEmail(email))) {
            return;
        }
        if ((!isValidatePassword(password))) {
            return;
        }
        if (agree.equalsIgnoreCase("0")){
            Toast.makeText(getApplicationContext(),"Please Accept Terms & conditions",Toast.LENGTH_SHORT).show();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        btnRegister.setEnabled(false);

        Call<BaseResponse> call = RetrofitClient.getInstance().getApi().userRegistrationRequest(name,email,mobile,password);

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                progressBar.setVisibility(View.GONE);
                btnRegister.setEnabled(true);

                BaseResponse registerResponse = response.body();


                if (registerResponse.getStatus().equalsIgnoreCase("10100")) {

                   /* int color = Color.GREEN;
                    snackBar(registerResponse.getMessage(), color);

*/
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            Intent intent = new Intent(getApplicationContext(), VerifyOtpActivity.class);
                            intent.putExtra("MobileNumber",mobile);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

                        }
                    }, 2000);




                }
                else if (registerResponse.getStatus().equals("10200")){
                    Toast.makeText(RegisterActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (registerResponse.getStatus().equals("10300")){
                    Toast.makeText(RegisterActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else if (registerResponse.getStatus().equals("10400")){
                    Toast.makeText(RegisterActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
               /* else{
                    int color = Color.RED;
                    snackBar(registerResponse.getMessage(), color);

                }*/


            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                btnRegister.setEnabled(true);
            }
        });
    }



    @OnClick({R.id.btnRegister, R.id.txtSignin})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnRegister:

                // Method to manually check connection status
               boolean isConnected = app.isConnection();
                if (isConnected) {
                    userRegistartion();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;
                    snackBar(message, color);
                }

                break;

            case R.id.txtSignin:
                Intent intent1 = new Intent(RegisterActivity.this, LoginActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;

            /*case R.id.txtVerify:
                // Method to manually check connection status
                isConnected = ConnectivityReceiver.isConnected();
                if (isConnected) {
                    userOtpRequest();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;
                    snackBar(message, color);
                }

                break;*/
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener

        AppController.getInstance().setConnectivityListener(this);
    }


    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;

        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
        }

        snackBar(message, color);


    }


    // snackBar
    private void snackBar(String message, int color) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parentLayout), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }


    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }


    // validate name
    private boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(name);

        if (name.isEmpty()) {
            userTil.setError("name is required");
            requestFocus(etUsername);
            return false;
        } else if (!matcher.matches()) {
            userTil.setError("Enter Alphabets Only");
            requestFocus(etUsername);
            return false;
        }
        else if (name.length() < 5 || name.length() > 20) {
            userTil.setError("Name Should be 5 to 20 characters");
            requestFocus(etUsername);
            return false;
        } else {
            userTil.setErrorEnabled(false);
        }
        return matcher.matches();
    }


    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            mobileTil.setError("Phone no is required");
            requestFocus(etPhone);

            return false;
        } else if (!matcher.matches()) {
            mobileTil.setError("Enter a valid mobile");
            requestFocus(etPhone);
            return false;
        } else {
            mobileTil.setErrorEnabled(false);
        }

        return matcher.matches();
    }



    // validate your email address
    private boolean isValidEmail(String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        if (email.isEmpty()) {
            emailTil.setError("Email is required");
            requestFocus(etEmail);
            return false;
        } else if (!matcher.matches()) {
            emailTil.setError("Enter a valid email");
            requestFocus(etEmail);
            return false;
        } else {
            emailTil.setErrorEnabled(false);
        }


        return matcher.matches();
    }



    // validate password
    private boolean isValidatePassword(String password) {
        if (password.isEmpty()) {
            pwdTil.setError("Password required");
            requestFocus(etPassword);
            return false;
        } else if (password.length()< 6 || password.length() > 20) {
            pwdTil.setError("Password Should be 6 to 20 characters");
            requestFocus(etPassword);
            return false;
        } else {
            pwdTil.setErrorEnabled(false);
        }

        return true;
    }


    // request focus
    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    // text input layout class
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etUsername:
                    isValidName(etUsername.getText().toString().trim());
                    break;
                case R.id.etPhone:
                    isValidPhoneNumber(etPhone.getText().toString().trim());
                    break;
               /* case R.id.etOtp:
                    isValidOtp(etOtp.getText().toString().trim());
                    break;*/
                case R.id.etEmail:
                    isValidEmail(etEmail.getText().toString().trim());
                    break;
                case R.id.etPassword:
                    isValidatePassword(etPassword.getText().toString().trim());
                    break;
            }
        }
    }
}
